;(function () {
  'use strict';

  var secure = false;
  var protocol = secure ? 'https' : 'http';
  // Live version, for deployment.
  var host = 'www.collect.host';
  // Local version, for testing purposes.
  // var host = 'www.collect.local';

  angular.module('collect')
    .constant('config', {
      protocol: protocol,
      host: host,
      paths: {
        api: '/api/v1/',
        api_login: '/api/login_check',
        images: '/images/reports/'
      }
    });

})();
