;(function () {
  'use strict';

  // Module declarations
  angular.module('collect', [
    'ionic',
    'ngCordova',
    'ngResource',
    'http-auth-interceptor',
    'ui-leaflet',
    // Modules
    'collect.security',
    'collect.reports',
    'collect.leaderboard',
    'collect.account',
    'collect.services'
  ]);

})();
