;(function () {
  'use strict';

  angular.module('collect')
    .config(Config);

  /* @ngInject */
  function Config(
    // Angular
    $compileProvider,
    $httpProvider,
    $urlRouterProvider,
    $stateProvider
) {
    // Allow 'app:' as protocol (for use in Hybrid Mobile apps)
    $compileProvider
      .aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|tel|file|app):/)
      .imgSrcSanitizationWhitelist(/^\s*((https?|ftp|file|app):|data:image\/)/)
    ;

    // Welcome, Login and Registration
    $stateProvider
      .state('welcome', {
        url: '/welcome',
        templateUrl: "templates/common/welcome.view.html",
        controller: "SecurityCtrl as vm"
      })
      .state('login', {
        url: '/login',
        templateUrl: "templates/security/login.view.html",
        controller: "SecurityCtrl as vm"
      })
      .state('register', {
        url: '/register',
        templateUrl: "templates/security/register.view.html",
        controller: "SecurityCtrl as vm"
      })

      .state('tabs', {
        url: '/tab',
        abstract: true, // setup an abstract state for the tabs directive
        templateUrl: function() {
          if (ionic.Platform.isAndroid()) {
            return "templates/common/tabs.android.view.html";
          }
          return "templates/common/tabs.view.html";
        }
      })

      // Reports (Overview, Create)
      .state('tabs.reports', {
        url: '/reports',
        views: {
          'tab-reports': {
            templateUrl: 'templates/reports/reports.view.html',
            controller: 'ReportsCtrl as vm',
          }
        }
      })
      .state('tabs.report-new', {
        url: '/new',
        views: {
          'tab-reports': {
            templateUrl: 'templates/reports/report-new.view.html',
            controller: 'ReportCreateCtrl as vm'
          }
        }
      })

      // Report Detail (Detail, Edit)
      .state('report-detail', {
        url: '/report/:report_id',
        templateUrl: 'templates/reports/report-detail.view.html',
        controller: 'ReportDetailCtrl as vm'
        }
      )
      .state('report-edit', {
        url: '/report/:report_id/edit',
        templateUrl: 'templates/reports/report-edit.view.html',
        controller: 'ReportEditCtrl as vm'
        }
      )

      // Leaderboard
      .state('tabs.leaderboard', {
        url: '/leaderboard',
        views: {
          'tab-leaderboard': {
            templateUrl: 'templates/leaderboard/leaderboard.view.html',
            controller: 'LeaderboardCtrl as vm',
          }
        }
      })

      // Account (Overview, Edit)
      .state('tabs.account', {
        url: '/account',
        views: {
          'tab-account': {
            templateUrl: 'templates/account/account.view.html',
            controller: 'AccountCtrl as vm',
          }
        }
      })
      .state('tabs.account-edit', {
        url: '/account/edit',
        views: {
          'tab-account': {
            templateUrl: 'templates/account/account-edit.view.html',
            controller: 'AccountEditCtrl as vm',
          }
        }
      })
    ;

    // Default
    $urlRouterProvider.otherwise('/welcome');
  }

})();
