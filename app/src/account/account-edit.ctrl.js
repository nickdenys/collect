;(function () {
  'use strict';

  angular.module('collect.account')
    .controller('AccountEditCtrl', AccountEditCtrl);

  /* @ngInject */
  function AccountEditCtrl(
    // Angular
    $log,
    $scope,
    $state,
    $rootScope,
    // Ionic
    $ionicLoading,
    // Custom
    AlertService,
    AccountResourceFactory
) {
    // ViewModel
    // =========
    var vm = this;
    vm.title = 'Edit Account';
    vm.loading = false;
    vm.updateAccount = updateAccount;
    vm.user = fetchUser();

    // Functions
    // =========

    function fetchUser() {
      vm.loading = true;

      var params = {
        user: $rootScope.currentUserName
      };

      return AccountResourceFactory
        .get(
          params,
          fetchUserSuccess,
          fetchUserError
        );

      function fetchUserSuccess(resource, responseHeader) {
        vm.loading = false;
      }

      function fetchUserError(error) {
        $log.error('fetchUser Error:', error);
      }
    }

    function updateAccount() {
      if(vm.user.password !== vm.user.password_repeat) {
        AlertService.error(
          "Passwords don't match!",
          function() {
            vm.user.password = vm.user.password_repeat = null;
          }
        );
      } else {
        vm.loading = true;

        var user = {
          firstName: vm.user.first_name,
          lastName: vm.user.last_name,
          email: vm.user.email,
        };
        if (vm.user.password && vm.user.password_repeat) {
          user.plainPassword = vm.user.password;
        }

        var postData = {
          user: user
        };

        var params = {
          user: $rootScope.currentUser.id,
        };

        return AccountResourceFactory
          .update(
            params,
            postData,
            updateSuccess,
            updateError
          );

        function updateSuccess(response, responseHeader) {
          $log.log('UpdateAccount Success', response);
          $state.go('tabs.account');
        }

        function updateError(error) {
          $log.error('UpdateAccount Error', error);
        }
      }
    }
  }

})();
