;(function () {
  'use strict';

  angular.module('collect.account')
    .controller('AccountCtrl', AccountCtrl);

  /* @ngInject */
  function AccountCtrl(
    // Angular
    $log,
    $scope,
    $rootScope,
    $state,
    // Ionic
    $ionicLoading,
    // Custom
    AuthenticationService,
    AccountResourceFactory
) {
    // ViewModel
    // =========
    var vm = this;

    vm.title = 'Account';
    $scope.$on('$ionicView.enter', function() {
      vm.user = fetchUserDetails();
    })
    vm.editUser = editUser;
    vm.logout = logoutUser;

    // Functions
    // =========
    function fetchUserDetails() {
      vm.loading = true;

      var params = {
        user: $rootScope.currentUserName
      };

      return AccountResourceFactory
        .get(
          params,
          fetchUserSuccess,
          fetchUserError
        );

      function fetchUserSuccess(response) {
        vm.loading = false;
        $rootScope.currentUser = response;
        return response;
      }

      function fetchUserError(error) {
        $log.error('fetchUserDetails Error:', error);
      }
    }

    function editUser() {
      $state.go('tabs.account-edit');
    }

    function logoutUser() {
      AuthenticationService.logout();
    }
  }

})();
