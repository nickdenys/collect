;(function () {
  'use strict';

  angular.module('collect.account')
    .factory('AccountResourceFactory', AccountResourceFactory);

  /* @ngInject */
  function AccountResourceFactory(
    // Angular
    $resource,
    // Custom
    UriFactory
) {
    var url = UriFactory.getApi('users/:user.:format');

    var paramDefaults = {
      user  : '@user',
      format: 'json'
    };

    var actions = {
      'update': {
        method: 'PUT'
      }
    };

    return $resource(url, paramDefaults, actions);
  }

})();
