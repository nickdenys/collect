;(function () {
  'use strict';

  angular.module('collect.security')
    .factory('RegisterResourceFactory', RegisterResourceFactory);

  /* @ngInject */
  function RegisterResourceFactory(
    // Angular
    $resource,
    // Custom
    UriFactory
) {
    var url = UriFactory.getApi('register.:format');

    var paramDefaults = {
      format    : 'json'
    };

    var actions = {
      'create': {
        method: 'POST'
      }
    };

    return $resource(url, paramDefaults, actions);
  }

})();
