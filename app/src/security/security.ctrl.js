;(function () {
  'use strict';

  angular.module('collect.security')
    .controller('SecurityCtrl', SecurityCtrl);

  /* @ngInject */
  function SecurityCtrl(
    // Angular
    $log,
    $scope,
    $rootScope,
    $state,
    // Ionic
    $ionicLoading,
    // Custom
    AlertService,
    AuthenticationService,
    RegisterResourceFactory,
    AccountResourceFactory
) {
    // ViewModel
    // =========
    var vm = this;

    // Functions
    // =========

    $scope.goToLoginPage = function() {
      $state.go('login');
    };
    $scope.goToRegisterPage = function() {
      $state.go('register');
    };

    $scope.submitLogin = function(credentials) {
      $ionicLoading.show({
        template: 'Authenticating',
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 200,
        showDelay: 0
      });
      AuthenticationService.login(credentials);
    };
    $scope.submitRegister = function(newUser) {
      if(newUser.plainPassword == newUser.passwordRepeat) {
        $ionicLoading.show({
          template: 'Submitting credentials',
          animation: 'fade-in',
          showBackdrop: true,
          maxWidth: 200,
          showDelay: 0
        });

        var user = {
          firstName: newUser.firstName,
          lastName: newUser.lastName,
          username: newUser.username,
          email: newUser.email,
          plainPassword: newUser.plainPassword
        };

        var params = {
          user: user
        };

        return RegisterResourceFactory
          .create(
            params,
            registerSuccess,
            registerError
          );

      } else {
        $scope.showAlert('Error', 'Passwords do not match!');
      }
    }

    function registerError(error) {
      $log.error('submitRegister Error:', error);
    }

    function registerSuccess(resource, responseHeader) {
      $ionicLoading.hide();
      AlertService.success(
        "Registration succesful. You can now log in.",
        function() {
          $state.go('welcome');
        }
      );
    }

    $scope.$on('event:auth-loginConfirmed', function(event, data){
      $ionicLoading.hide();
      $rootScope.isLoggedin = true;
      fetchUserDetails();
      $state.go('tabs.reports');
    });

    $scope.$on('event:auth-loginFailed', function(event, data){
      $ionicLoading.hide();
      $rootScope.isLoggedin = false;
      $log.error('Login failed', data);
      AlertService.error(data.message);
    });

    $scope.$on('event:auth-loginCancelled', function(event, data){
      $ionicLoading.hide();
      $rootScope.isLoggedin = false;
    });

    function fetchUserDetails() {
      var params = {
        'user': $rootScope.currentUserName
      };

      return AccountResourceFactory
        .get(
          params,
          success,
          error
        );

      function error(error) {
        $log.error('fetchUserDetails Error:', error);
      }

      function success(response) {
        $ionicLoading.hide();
        $rootScope.currentUser = response;
        return response;
      }
    }
  }

})();
