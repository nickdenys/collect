;(function () {
  'use strict';

  angular.module('collect.leaderboard')
    .controller('LeaderboardCtrl', LeaderboardCtrl);

  /* @ngInject */
  function LeaderboardCtrl(
    // Angular
    $log,
    $scope,
    // Ionic
    $ionicLoading,
    // Custom
    LeaderboardResourceFactory
) {
    // ViewModel
    // =========
    var vm = this;

    vm.title = 'Leaderboard';
    vm.loading = true;
    vm.leaderboard = getLeaderboard();
    vm.refresh = function() {
      vm.leaderboard = getLeaderboard();
    };

    // Functions
    // =========

    // Leaderboard
    // -----------
    function getLeaderboard() {
      vm.loading = true;

      var params = null;

      return LeaderboardResourceFactory
        .query(
          params,
          getLeaderboardSuccess,
          getLeaderboardError
        );

      function getLeaderboardSuccess(response) {
        vm.loading = false;
        $scope.$broadcast('scroll.refreshComplete');
        return response;
      }

      function getLeaderboardError(error) {
        $log.error('getLeaderboard Error:', error);
      }
    }
  }

})();
