;(function () {
  'use strict';

  angular.module('collect.leaderboard')
    .factory('LeaderboardResourceFactory', LeaderboardResourceFactory);

  /* @ngInject */
  function LeaderboardResourceFactory(
    // Angular
    $resource,
    // Custom
    UriFactory
) {
    var url = UriFactory.getApi('leaderboard.:format');

    var paramDefaults = {
      format    : 'json'
    };

    return $resource(url, paramDefaults);
  }

})();
