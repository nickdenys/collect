;(function () {
  'use strict';

  angular.module('collect.reports')
    .controller('ReportCreateCtrl', ReportCreateCtrl);

  /* @ngInject */
  function ReportCreateCtrl(
    // Angular
    $log,
    $scope,
    $state,
    $rootScope,
    $timeout,
    // ngCordova
    $cordovaCamera,
    $cordovaGeolocation,
    $cordovaFileTransfer,
    // Custom
    ReportResourceFactory,
    AlertService,
    config
) {
    // ViewModel
    // =========
    var vm = this;
    vm.title = 'Create Report';
    vm.loading = {
      geo: false,
      report: false
    };
    vm.lastPhoto = null;
    vm.getPhoto = getPhoto;
    vm.getLocation = getLocation;
    vm.createReport = createReport;
    vm.newReport = {};
    vm.header = $rootScope.token;

    // Functions
    // =========
    function getPhoto() {
      var cameraOptions = {
        quality: 85,
        destinationType: Camera.DestinationType.NATIVE_URI,
        sourceType: Camera.PictureSourceType.CAMERA,
        encodingType: Camera.EncodingType.JPEG,
        allowEdit: false,
        targetWidth: 1000,
        targetHeight: 500,
        popoverOptions: CameraPopoverOptions,
        saveToPhotoAlbum: false,
  	    correctOrientation: true
      };

      $cordovaCamera
        .getPicture(cameraOptions)
        .then(getPhotoSuccess, getPhotoError);

      function getPhotoSuccess(imageURI) {
        vm.lastPhoto = imageURI;
        var image = document.getElementById('newReportImage');
        $log.log('getPhoto Success');
        image.src = imageURI;
      }

      function getPhotoError(error) {
        AlertService.error("Could not take a picture. Please try again.");
        $log.error('GetPhoto Error', error);
      }
    }

    function getLocation() {
      vm.loading.geo = true;
      var posOptions = {
        timeout: 10000,
        enableHighAccuracy: true
      }

      $cordovaGeolocation
        .getCurrentPosition(posOptions)
        .then(function(position) {
          vm.loading.geo = false;
          vm.newReport.latitude = position.coords.latitude;
          vm.newReport.longitude = position.coords.longitude;
          prepareMap();
        }, function(error) {
          vm.loading.geo = false;
          AlertService.error("Could not find your location. Turn on GPS on your phone and try again.");
          $log.error(error);
        })
      ;
    }

    function createReport() {
      vm.loading.report = true;

      var report = {
        title: vm.newReport.title,
        description: vm.newReport.description,
        latitude: vm.newReport.latitude,
        longitude: vm.newReport.longitude
      };

      var postData = {
        report: report
      };

      var params = {
        user_id: $rootScope.currentUser.id
      };

      return ReportResourceFactory
        .save(
          params,
          postData,
          createSuccess,
          createError
        );

      function createSuccess(response, responseHeader) {
        $log.log('CreateReport Success');
        var reportId = response.report.id;
        if(vm.lastPhoto != null) {
          uploadImage(vm.lastPhoto, responseHeader().location, reportId);
        } else {
          vm.loading.report = false;
          $state.go('report-detail', { report_id: reportId });
        }
      }

      function createError(error) {
        vm.loading.report = false;
        AlertService.error("Could not create report. Please try again.");
        $log.error('CreateReport Error', error);
      }
    }

    function uploadImage(image, location, reportId) {
      var jpeg = true;
      var server = location + '/file/',
          targetPath = vm.lastPhoto,
          options = {
            fileKey: 'imageFile',
            httpMethod: 'POST',
            mimeType: jpeg ? 'image/jpg' : 'image/png',
            headers: {
              "Authorization": $rootScope.token
            }
          },
          trustAllHosts = true;

      $cordovaFileTransfer.upload(server, targetPath, options, trustAllHosts)
        .then(
          uploadImageSuccess,
          uploadImageError,
          uploadImageProgress
        )
      ;

      function uploadImageSuccess(response) {
        vm.loading.report = false;
        $state.go('report-detail', { report_id: reportId });
      }

      function uploadImageError(error) {
        vm.loading.report = false;
        AlertService.error("Could not upload Image. Please try again.");
        $log.error('uploadImage Error', error);
      }

      function uploadImageProgress(progress) {
        $timeout(function () {
          vm.uploadProgress = (progress.loaded / progress.total) * 100;
        });
      }
    }

    function prepareMap() {
      vm.tiles = {
        url: 'http://{s}.tile.openstreetmap.se/hydda/full/{z}/{x}/{y}.png'
      };
      angular.extend($scope, {
        mapCenter: {
          lat: vm.newReport.latitude,
          lng: vm.newReport.longitude,
          zoom: 15
        },
        markers: {
          currentPosMarker: {
            lat: vm.newReport.latitude,
            lng: vm.newReport.longitude,
            message: 'Current Location',
            focus: true,
            draggable: false
          }
        },
        defaults: {
          scrollWheelZoom: false
        }
      });
    }
  }

})();
