;(function () {
  'use strict';

  angular.module('collect.reports')
    .factory('CommentDeleteResourceFactory', CommentDeleteResourceFactory);

  /* @ngInject */
  function CommentDeleteResourceFactory(
    // Angular
    $resource,
    // Custom
    UriFactory
) {
    var url = UriFactory.getApi('users/:user_id/comments/:comment_id.:format');

    var paramDefaults = {
      user_id    : '@id',
      comment_id : '@id',
      format     : 'json'
    };

    return $resource(url, paramDefaults);
  }

})();
