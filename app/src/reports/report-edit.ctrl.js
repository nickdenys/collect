;(function () {
  'use strict';

  angular.module('collect.reports')
    .controller('ReportEditCtrl', ReportEditCtrl);

  /* @ngInject */
  function ReportEditCtrl(
    // Angular
    $log,
    $scope,
    $state,
    $rootScope,
    $timeout,
    // Ionic
    $ionicLoading,
    // ngCordova
    $cordovaCamera,
    $cordovaGeolocation,
    $cordovaFileTransfer,
    ReportsResourceFactory,
    ReportDetailResourceFactory,
    config
) {
    // ViewModel
    // =========
    var vm = this;
    vm.title = 'Edit Report';
    vm.loading = {
      report: false,
      geo: false
    };
    vm.lastPhoto = null;
    vm.imagesUri = config.protocol + '://' + config.host + config.paths.images;
    vm.getPhoto = getPhoto;
    vm.getLocation = getLocation;
    vm.updateReport = updateReport;
    vm.report = fetchReport();
    vm.header = $rootScope.token;

    // Functions
    // =========

    function getPhoto() {
      var cameraOptions = {
        quality: 85,
        destinationType: Camera.DestinationType.FILE_URI,
        sourceType: Camera.PictureSourceType.CAMERA,
        encodingType: Camera.EncodingType.JPEG,
        allowEdit: false,
        targetWidth: 1000,
        targetHeight: 500,
        popoverOptions: CameraPopoverOptions,
        saveToPhotoAlbum: false,
        correctOrientation: true
      };

      $cordovaCamera
        .getPicture(cameraOptions)
        .then(getPhotoSuccess, getPhotoError);

      function getPhotoSuccess(imageURI) {
        vm.lastPhoto = imageURI;
        var image = document.getElementById('editReportImage');
        $log.log('getPhoto Success');
        image.src = imageURI;
      }

      function getPhotoError(error) {
        AlertService.error("Could not take a picture. Please try again.");
        $log.error('GetPhoto Error', error);
      }
    }

    function getLocation() {
      vm.loading.geo = true;
      var posOptions = {
        timeout: 10000,
        enableHighAccuracy: true
      }

      $cordovaGeolocation
        .getCurrentPosition(posOptions)
        .then(function(position) {
          vm.loading.geo = false;
          vm.report.latitude = position.coords.latitude;
          vm.report.longitude = position.coords.longitude;
          prepareMap();
        }, function(error) {
          vm.loading.geo = false;
          AlertService.error("Could not find your location. Turn on GPS on your phone and try again.");
          $log.error(error);
        })
      ;
    }

    function fetchReport() {
      vm.loading.report = true;

      var params = {
        report_id: $state.params.report_id
      };

      return ReportsResourceFactory
        .get(
          params,
          fetchReportSuccess,
          fetchReportError
        );

      function fetchReportSuccess(resource, responseHeader) {
        prepareMap();
        vm.loading.report = false;
      }

      function fetchReportError(error) {
        $log.error('getReport Error:', error);
      }
    }

    function updateReport() {
      vm.loading.report = true;

      var report = {
        title: vm.report.title,
        description: vm.report.description,
        latitude: vm.report.latitude,
        longitude: vm.report.longitude
      };

      var postData = {
        report: report
      };

      var params = {
        user_id: $rootScope.currentUser.id,
        report_id: $state.params.report_id
      };

      return ReportDetailResourceFactory
        .update(
          params,
          postData,
          updateSuccess,
          updateError
        );

      function updateSuccess(response, responseHeader) {
        vm.loading.report = false;
        $log.log('UpdateReport Success');
        var reportId = $state.params.report_id;
        if(vm.lastPhoto != null) {
          uploadImage(vm.lastPhoto, responseHeader().location, reportId);
        } else {
          $state.go('report-detail', { report_id: reportId });
        }
      }

      function updateError(error) {
        vm.loading.report = false;
        AlertService.error("Could not update report. Please try again.");
        $log.error('UpdateReport Error', error);
      }
    }

    function uploadImage(image, location, reportId) {
      var jpeg = true;
      var server = location + '/file/',
          targetPath = vm.lastPhoto,
          options = {
            fileKey: 'imageFile',
            httpMethod: 'POST',
            mimeType: jpeg ? 'image/jpg' : 'image/png',
            headers: {
              "Authorization": $rootScope.token
            }
          },
          trustAllHosts = true;

      $cordovaFileTransfer.upload(server, targetPath, options, trustAllHosts)
        .then(
          uploadImageSuccess,
          uploadImageError,
          uploadImageProgress
        )
      ;

      function uploadImageSuccess(response) {
        $log.log('uploadImage Success');
        $state.go('report-detail', { report_id: reportId });
      }

      function uploadImageError(error) {
        $log.error('uploadImage Error', error);
        vm.error = error;
      }

      function uploadImageProgress(progress) {
        $timeout(function () {
            vm.uploadProgress = (progress.loaded / progress.total) * 100;
        });
      }
    }

    function prepareMap() {
      vm.tiles = {
        url: 'http://{s}.tile.openstreetmap.se/hydda/full/{z}/{x}/{y}.png'
      };
      angular.extend($scope, {
        mapCenter: {
          lat: vm.report.latitude,
          lng: vm.report.longitude,
          zoom: 15
        },
        markers: {
          currentPosMarker: {
            lat: vm.report.latitude,
            lng: vm.report.longitude,
            message: vm.report.title,
            focus: true,
            draggable: false
          }
        },
        defaults: {
          scrollWheelZoom: false
        }
      });
    }
  }

})();
