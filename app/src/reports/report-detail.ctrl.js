;(function () {
  'use strict';

  angular.module('collect.reports')
    .controller('ReportDetailCtrl', ReportDetailCtrl);

  /* @ngInject */
  function ReportDetailCtrl(
    // Angular
    $log,
    $state,
    $scope,
    $rootScope,
    // Ionic
    $ionicLoading,
    $ionicModal,
    $ionicActionSheet,
    // Custom
    config,
    AlertService,
    ReportsResourceFactory,
    ReportDetailResourceFactory,
    CommentResourceFactory,
    CommentDeleteResourceFactory
) {
    // ViewModel
    // =========
    var vm = this;
    vm.title = 'Report Detail';
    vm.loading = true;
    vm.goBack = function() {
      $state.go('tabs.reports');
    };
    vm.currentUserId = $rootScope.currentUser.id;
    vm.imagesUri = config.protocol + '://' + config.host + config.paths.images;
    $scope.$on('$ionicView.enter', function() {
      vm.report = getReport();
    });
    vm.createComment = createComment;
    vm.sendComment = sendComment;
    vm.removeCommentWarning = function(id) {
      AlertService.confirm(
        "Delete Comment",
        "Are you sure?",
        removeComment(id)
      );
    };
    vm.currentUserId = $rootScope.currentUser.id;
    vm.toggleOptions = function() {
      vm.hideSheet = $ionicActionSheet.show({
        buttons: [
         { text: '<i class="icon ion-edit"></i> Edit Report' },
        ],
        destructiveText: '<i class="icon ion-trash-a assertive"></i> Delete Report',
        titleText: 'Options',
        cancelText: 'Cancel',
        buttonClicked: function(index) {
          if(index == 0) {
            editReport();
          }
        },
        destructiveButtonClicked: function() {
          AlertService.confirm(
            "Delete Report",
            "Are you sure? This will permanently delete the report and can't be undone.",
            deleteReport()
          );
        }
      });
    }
    vm.refreshReport = function() {
      vm.loading = true;
      vm.report = getReport();
    }

    $ionicModal.fromTemplateUrl('templates/reports/comment-new.view.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modal = modal;
    });

    // Functions
    // =========

    // Reports
    // -------
    function getReport() {
      if($scope.modal) {
        $scope.modal.hide(); // Hide the modal after posting a comment.
      }

      var params = {
        report_id: $state.params.report_id
      };

      return ReportsResourceFactory
        .get(
          params,
          getReportSuccess,
          getReportError
        );

      function getReportSuccess(resource, responseHeader) {
        prepareMap();
        vm.loading = false;
        $scope.$broadcast('scroll.refreshComplete');
      }

      function getReportError(error) {
        vm.loading = false;
        $log.error('getReport Error:', error);
      }
    }

    function editReport() {
      $state.go('report-edit', {report_id: $state.params.report_id});
    }

    function deleteReport() {
      return function() {
        vm.hideSheet();
        vm.loading = true;

        var params = {
          report_id: $state.params.report_id,
          user_id: $rootScope.currentUser.id
        };

        return ReportDetailResourceFactory
          .delete(
            params,
            deleteReportSucces,
            deleteReportError
          );

        function deleteReportSucces(resource, responseHeader) {
          vm.loading = false;
          $log.log('DeleteReport Success');
          $state.go('tabs.reports');
        }

        function deleteReportError(error) {
          $log.error(error);
        }
      }
    }

    function createComment() {
      $scope.modal.show();
      $scope.submitting = false;
    }

    function sendComment(newComment) {
      $scope.submitting = true;

      var params = {
        user_id   : $rootScope.currentUser.id,
        report_id : parseInt($state.params.report_id)
      }, postData = {
        comment   : newComment
      };

      return CommentResourceFactory
        .save(
          params,
          postData,
          newCommentSuccess,
          newCommentError
        );

      function newCommentSuccess(resource, responseHeader) {
        $scope.submitting = false;
        newComment.content = null;
        vm.report = getReport();
      }

      function newCommentError(error) {
        $log.error('NewComment Error', error);
      }
    }

    function removeComment(id) {
      return function() {
        var params = {
          user_id     : vm.currentUserId,
          comment_id  : id
        };

        return CommentDeleteResourceFactory
          .remove(
            params,
            deleteCommentSuccess,
            deleteCommentError
          );

        function deleteCommentSuccess(resource, responseHeader) {
          vm.report = getReport();
        }

        function deleteCommentError(error) {
          $log.error('DeleteComment Error', error);
        }
      }
    }

    function prepareMap() {
      vm.tiles = {
        url: 'http://{s}.tile.openstreetmap.se/hydda/full/{z}/{x}/{y}.png'
      };
      angular.extend($scope, {
        mapCenter: {
          lat: vm.report.latitude,
          lng: vm.report.longitude,
          zoom: 15
        },
        markers: {
          currentPosMarker: {
            lat: vm.report.latitude,
            lng: vm.report.longitude,
            message: vm.report.title,
            focus: true,
            draggable: false
          }
        },
        defaults: {
          scrollWheelZoom: false
        }
      });
    }
  }

})();
