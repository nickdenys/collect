;(function () {
  'use strict';

  angular.module('collect.reports')
    .factory('CommentResourceFactory', CommentResourceFactory);

  /* @ngInject */
  function CommentResourceFactory(
    // Angular
    $resource,
    // Custom
    UriFactory
) {
    var url = UriFactory.getApi('users/:user_id/comments/:report_id.:format');

    var paramDefaults = {
      user_id   : '@id',
      report_id : '@id',
      format    : 'json'
    };

    return $resource(url, paramDefaults);
  }

})();
