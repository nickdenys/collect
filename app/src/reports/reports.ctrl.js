;(function () {
  'use strict';

  angular.module('collect.reports')
    .controller('ReportsCtrl', ReportsCtrl);

  /* @ngInject */
  function ReportsCtrl(
    // Angular
    $log,
    $state,
    $scope,
    // Ionic
    $ionicLoading,
    // Custom
    config,
    ReportsResourceFactory
) {
    // ViewModel
    // =========
    var vm = this;

    vm.title = 'Reports';
    vm.platform = ionic.Platform.platform();
    vm.imagesUri = config.protocol + '://' + config.host + config.paths.images;
    vm.reports = getReports();
    vm.addReport = function() {
      $state.go('tabs.report-new');
    };
    vm.refreshReports = function() {
      vm.loading = false;
      vm.reports = getReports();
    }

    vm.loading = true;

    // Functions
    // =========

    // Reports
    // -------
    function getReports() {
      var params = null;

      return ReportsResourceFactory
        .query(
          params,
          getReportsSuccess,
          getReportsError
        );

      function getReportsSuccess(resource, responseHeader) {
        vm.loading = false;
        $scope.$broadcast('scroll.refreshComplete');
      }

      function getReportsError(error) {
        vm.loading = false;
        $scope.$broadcast('scroll.refreshComplete');
        $log.error('getReports Error:', error);
      }
    }
  }

})();
