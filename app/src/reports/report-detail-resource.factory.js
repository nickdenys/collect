;(function () {
  'use strict';

  angular.module('collect.reports')
    .factory('ReportDetailResourceFactory', ReportDetailResourceFactory);

  /* @ngInject */
  function ReportDetailResourceFactory(
    // Angular
    $resource,
    // Custom
    UriFactory
) {
    var url = UriFactory.getApi('users/:user_id/reports/:report_id.:format');

    var paramDefaults = {
      user_id   : '@id',
      report_id : '@id',
      format    : 'json'
    };

    var actions = {
      'update': {
        method: 'PUT'
      }
    };

    return $resource(url, paramDefaults, actions);
  }

})();
