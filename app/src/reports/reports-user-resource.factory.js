;(function () {
  'use strict';

  angular.module('collect.reports')
    .factory('ReportResourceFactory', ReportResourceFactory);

  /* @ngInject */
  function ReportResourceFactory(
    // Angular
    $resource,
    // Custom
    UriFactory
) {
    var url = UriFactory.getApi('users/:user_id/reports.:format');

    var paramDefaults = {
      user_id : '@id',
      format  : 'json'
    };

    return $resource(url, paramDefaults);
  }

})();
