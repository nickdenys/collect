;(function () {
  'use strict';

  angular.module('collect.reports')
    .factory('ReportsResourceFactory', ReportsResourceFactory);

  /* @ngInject */
  function ReportsResourceFactory(
    // Angular
    $resource,
    // Custom
    UriFactory
) {
    var url = UriFactory.getApi('reports/:report_id.:format');

    var paramDefaults = {
      report_id : '@id',
      format    : 'json'
    };

    return $resource(url, paramDefaults);
  }

})();
