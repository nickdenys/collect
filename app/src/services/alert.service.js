;(function () {
  'use strict';

  angular.module('collect.services')
    .factory('AlertService', AlertService);

  /* @ngInject */
  function AlertService(
    $log,
    // Ionic
    $ionicPopup
) {
    return {
      success: function(msg, callback) {
        var successAlert = $ionicPopup.alert({
          title: 'Success!',
          template: msg
        });

        successAlert.then(callback);
      },
      error: function(msg, callback) {
        var errorAlert = $ionicPopup.alert({
          title: 'Error!',
          template: msg
        });

        errorAlert.then(callback);
      },
      confirm: function(title, msg, callback) {
        var confirmPopup = $ionicPopup.confirm({
          title: title,
          template: msg
        });

        confirmPopup.then(function(res) {
          if(res) {
            callback();
          }
        })
      }
    }
  }

})();
