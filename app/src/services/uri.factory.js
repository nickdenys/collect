;(function () {
  'use strict';

  angular.module('collect.services')
    .factory('UriFactory', UriFactory);

  /* @ngInject */
  function UriFactory(
    // Angular
    $location,
    // Custom
    config
) {
    function getApi(path) {
      var protocol = config.protocol ? config.protocol : $location.protocol(),
          host     = config.host ? config.host : $location.host(),
          uri      = protocol + '://' + host + config.paths.api + path;

      return uri;
    }

    return {
      getApi: getApi
    };
  }

})();
