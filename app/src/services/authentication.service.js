;(function () {
  'use strict';

  angular.module('collect.services')
    .factory('AuthenticationService', AuthenticationService);

  /* @ngInject */
  function AuthenticationService(
    // Angular
    $rootScope,
    $http,
    $state,
    authService,
    $httpBackend,
    $location,
    // Ionic
    $ionicHistory,
    // Custom
    config
) {
    var uri = config.protocol + '://' + config.host + config.paths.api_login;

    return {
      login: function(credentials) {
        $http
          .post(uri, credentials, { ignoreAuthModule: true })
          .success(function(data, status, headers, config) {
            $http.defaults.headers.common.Authorization = 'Bearer ' + data.token;  // Step 1
            $rootScope.currentUserName = config.data.username;
            $rootScope.token = 'Bearer ' + data.token;
            authService.loginConfirmed(data, function(config) { // Step 2 + 3
              config.headers.Authorization = 'Bearer ' + data.token;
              return config;
            });
          })
          .error(function(data, status, headers, config) {
            $rootScope.$broadcast('event:auth-loginFailed', data);
          });
      },
      logout: function(user) {
        delete $http.defaults.headers.common.Authorization;
        $ionicHistory.nextViewOptions({
          disableBack: true
        });
        $state.go('welcome');
        $rootScope.$broadcast('event:auth-logoutComplete');
      }
    }
  }

})();
