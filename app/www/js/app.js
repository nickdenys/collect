;(function () {
  'use strict';

  // Module declarations
  angular.module('collect', [
    'ionic',
    'ngCordova',
    'ngResource',
    'http-auth-interceptor',
    'ui-leaflet',
    // Modules
    'collect.security',
    'collect.reports',
    'collect.leaderboard',
    'collect.account',
    'collect.services'
  ]);

})();

;(function () {
  'use strict';

  angular.module('collect.account', []);

})();

;(function () {
  'use strict';

  angular.module('collect.leaderboard', []);

})();

;(function () {
  'use strict';

  angular.module('collect.reports', []);

})();

;(function () {
  'use strict';

  angular.module('collect.security', []);

})();

;(function () {
  'use strict';

  angular.module('collect.services', []);

})();

;(function () {
  'use strict';

  Run.$inject = ['$log', '$ionicPlatform'];
  angular.module('collect')
    .run(Run);

  /* @ngInject */
  function Run(
    // Angular
    $log,
    // Ionic
    $ionicPlatform
) {
    $ionicPlatform.ready(whenReady);

    function whenReady() {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs).
      // The reason we default this to hidden is that native apps don't usually show an accessory bar, at
      // least on iOS. It's a dead giveaway that an app is using a Web View. However, it's sometimes
      // useful especially with forms, though we would prefer giving the user a little more room
      // to interact with the app.
      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);
      }

      if (window.StatusBar) {
        // Set the statusbar to use the default style, tweak this to
        // remove the status bar on iOS or change it to use white instead of dark colors.
        StatusBar.styleDefault();
      }
    }
  }

})();

;(function () {
  'use strict';

  Config.$inject = ['$compileProvider', '$httpProvider', '$urlRouterProvider', '$stateProvider'];
  angular.module('collect')
    .config(Config);

  /* @ngInject */
  function Config(
    // Angular
    $compileProvider,
    $httpProvider,
    $urlRouterProvider,
    $stateProvider
) {
    // Allow 'app:' as protocol (for use in Hybrid Mobile apps)
    $compileProvider
      .aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|tel|file|app):/)
      .imgSrcSanitizationWhitelist(/^\s*((https?|ftp|file|app):|data:image\/)/)
    ;

    // Welcome, Login and Registration
    $stateProvider
      .state('welcome', {
        url: '/welcome',
        templateUrl: "templates/common/welcome.view.html",
        controller: "SecurityCtrl as vm"
      })
      .state('login', {
        url: '/login',
        templateUrl: "templates/security/login.view.html",
        controller: "SecurityCtrl as vm"
      })
      .state('register', {
        url: '/register',
        templateUrl: "templates/security/register.view.html",
        controller: "SecurityCtrl as vm"
      })

      .state('tabs', {
        url: '/tab',
        abstract: true, // setup an abstract state for the tabs directive
        templateUrl: function() {
          if (ionic.Platform.isAndroid()) {
            return "templates/common/tabs.android.view.html";
          }
          return "templates/common/tabs.view.html";
        }
      })

      // Reports (Overview, Create)
      .state('tabs.reports', {
        url: '/reports',
        views: {
          'tab-reports': {
            templateUrl: 'templates/reports/reports.view.html',
            controller: 'ReportsCtrl as vm',
          }
        }
      })
      .state('tabs.report-new', {
        url: '/new',
        views: {
          'tab-reports': {
            templateUrl: 'templates/reports/report-new.view.html',
            controller: 'ReportCreateCtrl as vm'
          }
        }
      })

      // Report Detail (Detail, Edit)
      .state('report-detail', {
        url: '/report/:report_id',
        templateUrl: 'templates/reports/report-detail.view.html',
        controller: 'ReportDetailCtrl as vm'
        }
      )
      .state('report-edit', {
        url: '/report/:report_id/edit',
        templateUrl: 'templates/reports/report-edit.view.html',
        controller: 'ReportEditCtrl as vm'
        }
      )

      // Leaderboard
      .state('tabs.leaderboard', {
        url: '/leaderboard',
        views: {
          'tab-leaderboard': {
            templateUrl: 'templates/leaderboard/leaderboard.view.html',
            controller: 'LeaderboardCtrl as vm',
          }
        }
      })

      // Account (Overview, Edit)
      .state('tabs.account', {
        url: '/account',
        views: {
          'tab-account': {
            templateUrl: 'templates/account/account.view.html',
            controller: 'AccountCtrl as vm',
          }
        }
      })
      .state('tabs.account-edit', {
        url: '/account/edit',
        views: {
          'tab-account': {
            templateUrl: 'templates/account/account-edit.view.html',
            controller: 'AccountEditCtrl as vm',
          }
        }
      })
    ;

    // Default
    $urlRouterProvider.otherwise('/welcome');
  }

})();

;(function () {
  'use strict';

  var secure = false;
  var protocol = secure ? 'https' : 'http';
  // Live version, for deployment.
  var host = 'www.collect.host';
  // Local version, for testing purposes.
  // var host = 'www.collect.local';

  angular.module('collect')
    .constant('config', {
      protocol: protocol,
      host: host,
      paths: {
        api: '/api/v1/',
        api_login: '/api/login_check',
        images: '/images/reports/'
      }
    });

})();

;(function () {
  'use strict';

  AccountEditCtrl.$inject = ['$log', '$scope', '$state', '$rootScope', '$ionicLoading', 'AlertService', 'AccountResourceFactory'];
  angular.module('collect.account')
    .controller('AccountEditCtrl', AccountEditCtrl);

  /* @ngInject */
  function AccountEditCtrl(
    // Angular
    $log,
    $scope,
    $state,
    $rootScope,
    // Ionic
    $ionicLoading,
    // Custom
    AlertService,
    AccountResourceFactory
) {
    // ViewModel
    // =========
    var vm = this;
    vm.title = 'Edit Account';
    vm.loading = false;
    vm.updateAccount = updateAccount;
    vm.user = fetchUser();

    // Functions
    // =========

    function fetchUser() {
      vm.loading = true;

      var params = {
        user: $rootScope.currentUserName
      };

      return AccountResourceFactory
        .get(
          params,
          fetchUserSuccess,
          fetchUserError
        );

      function fetchUserSuccess(resource, responseHeader) {
        vm.loading = false;
      }

      function fetchUserError(error) {
        $log.error('fetchUser Error:', error);
      }
    }

    function updateAccount() {
      if(vm.user.password !== vm.user.password_repeat) {
        AlertService.error(
          "Passwords don't match!",
          function() {
            vm.user.password = vm.user.password_repeat = null;
          }
        );
      } else {
        vm.loading = true;

        var user = {
          firstName: vm.user.first_name,
          lastName: vm.user.last_name,
          email: vm.user.email,
        };
        if (vm.user.password && vm.user.password_repeat) {
          user.plainPassword = vm.user.password;
        }

        var postData = {
          user: user
        };

        var params = {
          user: $rootScope.currentUser.id,
        };

        return AccountResourceFactory
          .update(
            params,
            postData,
            updateSuccess,
            updateError
          );

        function updateSuccess(response, responseHeader) {
          $log.log('UpdateAccount Success', response);
          $state.go('tabs.account');
        }

        function updateError(error) {
          $log.error('UpdateAccount Error', error);
        }
      }
    }
  }

})();

;(function () {
  'use strict';

  AccountResourceFactory.$inject = ['$resource', 'UriFactory'];
  angular.module('collect.account')
    .factory('AccountResourceFactory', AccountResourceFactory);

  /* @ngInject */
  function AccountResourceFactory(
    // Angular
    $resource,
    // Custom
    UriFactory
) {
    var url = UriFactory.getApi('users/:user.:format');

    var paramDefaults = {
      user  : '@user',
      format: 'json'
    };

    var actions = {
      'update': {
        method: 'PUT'
      }
    };

    return $resource(url, paramDefaults, actions);
  }

})();

;(function () {
  'use strict';

  AccountCtrl.$inject = ['$log', '$scope', '$rootScope', '$state', '$ionicLoading', 'AuthenticationService', 'AccountResourceFactory'];
  angular.module('collect.account')
    .controller('AccountCtrl', AccountCtrl);

  /* @ngInject */
  function AccountCtrl(
    // Angular
    $log,
    $scope,
    $rootScope,
    $state,
    // Ionic
    $ionicLoading,
    // Custom
    AuthenticationService,
    AccountResourceFactory
) {
    // ViewModel
    // =========
    var vm = this;

    vm.title = 'Account';
    $scope.$on('$ionicView.enter', function() {
      vm.user = fetchUserDetails();
    })
    vm.editUser = editUser;
    vm.logout = logoutUser;

    // Functions
    // =========
    function fetchUserDetails() {
      vm.loading = true;

      var params = {
        user: $rootScope.currentUserName
      };

      return AccountResourceFactory
        .get(
          params,
          fetchUserSuccess,
          fetchUserError
        );

      function fetchUserSuccess(response) {
        vm.loading = false;
        $rootScope.currentUser = response;
        return response;
      }

      function fetchUserError(error) {
        $log.error('fetchUserDetails Error:', error);
      }
    }

    function editUser() {
      $state.go('tabs.account-edit');
    }

    function logoutUser() {
      AuthenticationService.logout();
    }
  }

})();

;(function () {
  'use strict';

  LeaderboardResourceFactory.$inject = ['$resource', 'UriFactory'];
  angular.module('collect.leaderboard')
    .factory('LeaderboardResourceFactory', LeaderboardResourceFactory);

  /* @ngInject */
  function LeaderboardResourceFactory(
    // Angular
    $resource,
    // Custom
    UriFactory
) {
    var url = UriFactory.getApi('leaderboard.:format');

    var paramDefaults = {
      format    : 'json'
    };

    return $resource(url, paramDefaults);
  }

})();

;(function () {
  'use strict';

  LeaderboardCtrl.$inject = ['$log', '$scope', '$ionicLoading', 'LeaderboardResourceFactory'];
  angular.module('collect.leaderboard')
    .controller('LeaderboardCtrl', LeaderboardCtrl);

  /* @ngInject */
  function LeaderboardCtrl(
    // Angular
    $log,
    $scope,
    // Ionic
    $ionicLoading,
    // Custom
    LeaderboardResourceFactory
) {
    // ViewModel
    // =========
    var vm = this;

    vm.title = 'Leaderboard';
    vm.loading = true;
    vm.leaderboard = getLeaderboard();
    vm.refresh = function() {
      vm.leaderboard = getLeaderboard();
    };

    // Functions
    // =========

    // Leaderboard
    // -----------
    function getLeaderboard() {
      vm.loading = true;

      var params = null;

      return LeaderboardResourceFactory
        .query(
          params,
          getLeaderboardSuccess,
          getLeaderboardError
        );

      function getLeaderboardSuccess(response) {
        vm.loading = false;
        $scope.$broadcast('scroll.refreshComplete');
        return response;
      }

      function getLeaderboardError(error) {
        $log.error('getLeaderboard Error:', error);
      }
    }
  }

})();

;(function () {
  'use strict';

  CommentDeleteResourceFactory.$inject = ['$resource', 'UriFactory'];
  angular.module('collect.reports')
    .factory('CommentDeleteResourceFactory', CommentDeleteResourceFactory);

  /* @ngInject */
  function CommentDeleteResourceFactory(
    // Angular
    $resource,
    // Custom
    UriFactory
) {
    var url = UriFactory.getApi('users/:user_id/comments/:comment_id.:format');

    var paramDefaults = {
      user_id    : '@id',
      comment_id : '@id',
      format     : 'json'
    };

    return $resource(url, paramDefaults);
  }

})();

;(function () {
  'use strict';

  CommentResourceFactory.$inject = ['$resource', 'UriFactory'];
  angular.module('collect.reports')
    .factory('CommentResourceFactory', CommentResourceFactory);

  /* @ngInject */
  function CommentResourceFactory(
    // Angular
    $resource,
    // Custom
    UriFactory
) {
    var url = UriFactory.getApi('users/:user_id/comments/:report_id.:format');

    var paramDefaults = {
      user_id   : '@id',
      report_id : '@id',
      format    : 'json'
    };

    return $resource(url, paramDefaults);
  }

})();

;(function () {
  'use strict';

  ReportDetailResourceFactory.$inject = ['$resource', 'UriFactory'];
  angular.module('collect.reports')
    .factory('ReportDetailResourceFactory', ReportDetailResourceFactory);

  /* @ngInject */
  function ReportDetailResourceFactory(
    // Angular
    $resource,
    // Custom
    UriFactory
) {
    var url = UriFactory.getApi('users/:user_id/reports/:report_id.:format');

    var paramDefaults = {
      user_id   : '@id',
      report_id : '@id',
      format    : 'json'
    };

    var actions = {
      'update': {
        method: 'PUT'
      }
    };

    return $resource(url, paramDefaults, actions);
  }

})();

;(function () {
  'use strict';

  ReportDetailCtrl.$inject = ['$log', '$state', '$scope', '$rootScope', '$ionicLoading', '$ionicModal', '$ionicActionSheet', 'config', 'AlertService', 'ReportsResourceFactory', 'ReportDetailResourceFactory', 'CommentResourceFactory', 'CommentDeleteResourceFactory'];
  angular.module('collect.reports')
    .controller('ReportDetailCtrl', ReportDetailCtrl);

  /* @ngInject */
  function ReportDetailCtrl(
    // Angular
    $log,
    $state,
    $scope,
    $rootScope,
    // Ionic
    $ionicLoading,
    $ionicModal,
    $ionicActionSheet,
    // Custom
    config,
    AlertService,
    ReportsResourceFactory,
    ReportDetailResourceFactory,
    CommentResourceFactory,
    CommentDeleteResourceFactory
) {
    // ViewModel
    // =========
    var vm = this;
    vm.title = 'Report Detail';
    vm.loading = true;
    vm.goBack = function() {
      $state.go('tabs.reports');
    };
    vm.currentUserId = $rootScope.currentUser.id;
    vm.imagesUri = config.protocol + '://' + config.host + config.paths.images;
    $scope.$on('$ionicView.enter', function() {
      vm.report = getReport();
    });
    vm.createComment = createComment;
    vm.sendComment = sendComment;
    vm.removeCommentWarning = function(id) {
      AlertService.confirm(
        "Delete Comment",
        "Are you sure?",
        removeComment(id)
      );
    };
    vm.currentUserId = $rootScope.currentUser.id;
    vm.toggleOptions = function() {
      vm.hideSheet = $ionicActionSheet.show({
        buttons: [
         { text: '<i class="icon ion-edit"></i> Edit Report' },
        ],
        destructiveText: '<i class="icon ion-trash-a assertive"></i> Delete Report',
        titleText: 'Options',
        cancelText: 'Cancel',
        buttonClicked: function(index) {
          if(index == 0) {
            editReport();
          }
        },
        destructiveButtonClicked: function() {
          AlertService.confirm(
            "Delete Report",
            "Are you sure? This will permanently delete the report and can't be undone.",
            deleteReport()
          );
        }
      });
    }
    vm.refreshReport = function() {
      vm.loading = true;
      vm.report = getReport();
    }

    $ionicModal.fromTemplateUrl('templates/reports/comment-new.view.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modal = modal;
    });

    // Functions
    // =========

    // Reports
    // -------
    function getReport() {
      if($scope.modal) {
        $scope.modal.hide(); // Hide the modal after posting a comment.
      }

      var params = {
        report_id: $state.params.report_id
      };

      return ReportsResourceFactory
        .get(
          params,
          getReportSuccess,
          getReportError
        );

      function getReportSuccess(resource, responseHeader) {
        prepareMap();
        vm.loading = false;
        $scope.$broadcast('scroll.refreshComplete');
      }

      function getReportError(error) {
        vm.loading = false;
        $log.error('getReport Error:', error);
      }
    }

    function editReport() {
      $state.go('report-edit', {report_id: $state.params.report_id});
    }

    function deleteReport() {
      return function() {
        vm.hideSheet();
        vm.loading = true;

        var params = {
          report_id: $state.params.report_id,
          user_id: $rootScope.currentUser.id
        };

        return ReportDetailResourceFactory
          .delete(
            params,
            deleteReportSucces,
            deleteReportError
          );

        function deleteReportSucces(resource, responseHeader) {
          vm.loading = false;
          $log.log('DeleteReport Success');
          $state.go('tabs.reports');
        }

        function deleteReportError(error) {
          $log.error(error);
        }
      }
    }

    function createComment() {
      $scope.modal.show();
      $scope.submitting = false;
    }

    function sendComment(newComment) {
      $scope.submitting = true;

      var params = {
        user_id   : $rootScope.currentUser.id,
        report_id : parseInt($state.params.report_id)
      }, postData = {
        comment   : newComment
      };

      return CommentResourceFactory
        .save(
          params,
          postData,
          newCommentSuccess,
          newCommentError
        );

      function newCommentSuccess(resource, responseHeader) {
        $scope.submitting = false;
        newComment.content = null;
        vm.report = getReport();
      }

      function newCommentError(error) {
        $log.error('NewComment Error', error);
      }
    }

    function removeComment(id) {
      return function() {
        var params = {
          user_id     : vm.currentUserId,
          comment_id  : id
        };

        return CommentDeleteResourceFactory
          .remove(
            params,
            deleteCommentSuccess,
            deleteCommentError
          );

        function deleteCommentSuccess(resource, responseHeader) {
          vm.report = getReport();
        }

        function deleteCommentError(error) {
          $log.error('DeleteComment Error', error);
        }
      }
    }

    function prepareMap() {
      vm.tiles = {
        url: 'http://{s}.tile.openstreetmap.se/hydda/full/{z}/{x}/{y}.png'
      };
      angular.extend($scope, {
        mapCenter: {
          lat: vm.report.latitude,
          lng: vm.report.longitude,
          zoom: 15
        },
        markers: {
          currentPosMarker: {
            lat: vm.report.latitude,
            lng: vm.report.longitude,
            message: vm.report.title,
            focus: true,
            draggable: false
          }
        },
        defaults: {
          scrollWheelZoom: false
        }
      });
    }
  }

})();

;(function () {
  'use strict';

  ReportEditCtrl.$inject = ['$log', '$scope', '$state', '$rootScope', '$timeout', '$ionicLoading', '$cordovaCamera', '$cordovaGeolocation', '$cordovaFileTransfer', 'ReportsResourceFactory', 'ReportDetailResourceFactory', 'config'];
  angular.module('collect.reports')
    .controller('ReportEditCtrl', ReportEditCtrl);

  /* @ngInject */
  function ReportEditCtrl(
    // Angular
    $log,
    $scope,
    $state,
    $rootScope,
    $timeout,
    // Ionic
    $ionicLoading,
    // ngCordova
    $cordovaCamera,
    $cordovaGeolocation,
    $cordovaFileTransfer,
    ReportsResourceFactory,
    ReportDetailResourceFactory,
    config
) {
    // ViewModel
    // =========
    var vm = this;
    vm.title = 'Edit Report';
    vm.loading = {
      report: false,
      geo: false
    };
    vm.lastPhoto = null;
    vm.imagesUri = config.protocol + '://' + config.host + config.paths.images;
    vm.getPhoto = getPhoto;
    vm.getLocation = getLocation;
    vm.updateReport = updateReport;
    vm.report = fetchReport();
    vm.header = $rootScope.token;

    // Functions
    // =========

    function getPhoto() {
      var cameraOptions = {
        quality: 85,
        destinationType: Camera.DestinationType.FILE_URI,
        sourceType: Camera.PictureSourceType.CAMERA,
        encodingType: Camera.EncodingType.JPEG,
        allowEdit: false,
        targetWidth: 1000,
        targetHeight: 500,
        popoverOptions: CameraPopoverOptions,
        saveToPhotoAlbum: false,
        correctOrientation: true
      };

      $cordovaCamera
        .getPicture(cameraOptions)
        .then(getPhotoSuccess, getPhotoError);

      function getPhotoSuccess(imageURI) {
        vm.lastPhoto = imageURI;
        var image = document.getElementById('editReportImage');
        $log.log('getPhoto Success');
        image.src = imageURI;
      }

      function getPhotoError(error) {
        AlertService.error("Could not take a picture. Please try again.");
        $log.error('GetPhoto Error', error);
      }
    }

    function getLocation() {
      vm.loading.geo = true;
      var posOptions = {
        timeout: 10000,
        enableHighAccuracy: true
      }

      $cordovaGeolocation
        .getCurrentPosition(posOptions)
        .then(function(position) {
          vm.loading.geo = false;
          vm.report.latitude = position.coords.latitude;
          vm.report.longitude = position.coords.longitude;
          prepareMap();
        }, function(error) {
          vm.loading.geo = false;
          AlertService.error("Could not find your location. Turn on GPS on your phone and try again.");
          $log.error(error);
        })
      ;
    }

    function fetchReport() {
      vm.loading.report = true;

      var params = {
        report_id: $state.params.report_id
      };

      return ReportsResourceFactory
        .get(
          params,
          fetchReportSuccess,
          fetchReportError
        );

      function fetchReportSuccess(resource, responseHeader) {
        prepareMap();
        vm.loading.report = false;
      }

      function fetchReportError(error) {
        $log.error('getReport Error:', error);
      }
    }

    function updateReport() {
      vm.loading.report = true;

      var report = {
        title: vm.report.title,
        description: vm.report.description,
        latitude: vm.report.latitude,
        longitude: vm.report.longitude
      };

      var postData = {
        report: report
      };

      var params = {
        user_id: $rootScope.currentUser.id,
        report_id: $state.params.report_id
      };

      return ReportDetailResourceFactory
        .update(
          params,
          postData,
          updateSuccess,
          updateError
        );

      function updateSuccess(response, responseHeader) {
        vm.loading.report = false;
        $log.log('UpdateReport Success');
        var reportId = $state.params.report_id;
        if(vm.lastPhoto != null) {
          uploadImage(vm.lastPhoto, responseHeader().location, reportId);
        } else {
          $state.go('report-detail', { report_id: reportId });
        }
      }

      function updateError(error) {
        vm.loading.report = false;
        AlertService.error("Could not update report. Please try again.");
        $log.error('UpdateReport Error', error);
      }
    }

    function uploadImage(image, location, reportId) {
      var jpeg = true;
      var server = location + '/file/',
          targetPath = vm.lastPhoto,
          options = {
            fileKey: 'imageFile',
            httpMethod: 'POST',
            mimeType: jpeg ? 'image/jpg' : 'image/png',
            headers: {
              "Authorization": $rootScope.token
            }
          },
          trustAllHosts = true;

      $cordovaFileTransfer.upload(server, targetPath, options, trustAllHosts)
        .then(
          uploadImageSuccess,
          uploadImageError,
          uploadImageProgress
        )
      ;

      function uploadImageSuccess(response) {
        $log.log('uploadImage Success');
        $state.go('report-detail', { report_id: reportId });
      }

      function uploadImageError(error) {
        $log.error('uploadImage Error', error);
        vm.error = error;
      }

      function uploadImageProgress(progress) {
        $timeout(function () {
            vm.uploadProgress = (progress.loaded / progress.total) * 100;
        });
      }
    }

    function prepareMap() {
      vm.tiles = {
        url: 'http://{s}.tile.openstreetmap.se/hydda/full/{z}/{x}/{y}.png'
      };
      angular.extend($scope, {
        mapCenter: {
          lat: vm.report.latitude,
          lng: vm.report.longitude,
          zoom: 15
        },
        markers: {
          currentPosMarker: {
            lat: vm.report.latitude,
            lng: vm.report.longitude,
            message: vm.report.title,
            focus: true,
            draggable: false
          }
        },
        defaults: {
          scrollWheelZoom: false
        }
      });
    }
  }

})();

;(function () {
  'use strict';

  ReportCreateCtrl.$inject = ['$log', '$scope', '$state', '$rootScope', '$timeout', '$cordovaCamera', '$cordovaGeolocation', '$cordovaFileTransfer', 'ReportResourceFactory', 'AlertService', 'config'];
  angular.module('collect.reports')
    .controller('ReportCreateCtrl', ReportCreateCtrl);

  /* @ngInject */
  function ReportCreateCtrl(
    // Angular
    $log,
    $scope,
    $state,
    $rootScope,
    $timeout,
    // ngCordova
    $cordovaCamera,
    $cordovaGeolocation,
    $cordovaFileTransfer,
    // Custom
    ReportResourceFactory,
    AlertService,
    config
) {
    // ViewModel
    // =========
    var vm = this;
    vm.title = 'Create Report';
    vm.loading = {
      geo: false,
      report: false
    };
    vm.lastPhoto = null;
    vm.getPhoto = getPhoto;
    vm.getLocation = getLocation;
    vm.createReport = createReport;
    vm.newReport = {};
    vm.header = $rootScope.token;

    // Functions
    // =========
    function getPhoto() {
      var cameraOptions = {
        quality: 85,
        destinationType: Camera.DestinationType.NATIVE_URI,
        sourceType: Camera.PictureSourceType.CAMERA,
        encodingType: Camera.EncodingType.JPEG,
        allowEdit: false,
        targetWidth: 1000,
        targetHeight: 500,
        popoverOptions: CameraPopoverOptions,
        saveToPhotoAlbum: false,
  	    correctOrientation: true
      };

      $cordovaCamera
        .getPicture(cameraOptions)
        .then(getPhotoSuccess, getPhotoError);

      function getPhotoSuccess(imageURI) {
        vm.lastPhoto = imageURI;
        var image = document.getElementById('newReportImage');
        $log.log('getPhoto Success');
        image.src = imageURI;
      }

      function getPhotoError(error) {
        AlertService.error("Could not take a picture. Please try again.");
        $log.error('GetPhoto Error', error);
      }
    }

    function getLocation() {
      vm.loading.geo = true;
      var posOptions = {
        timeout: 10000,
        enableHighAccuracy: true
      }

      $cordovaGeolocation
        .getCurrentPosition(posOptions)
        .then(function(position) {
          vm.loading.geo = false;
          vm.newReport.latitude = position.coords.latitude;
          vm.newReport.longitude = position.coords.longitude;
          prepareMap();
        }, function(error) {
          vm.loading.geo = false;
          AlertService.error("Could not find your location. Turn on GPS on your phone and try again.");
          $log.error(error);
        })
      ;
    }

    function createReport() {
      vm.loading.report = true;

      var report = {
        title: vm.newReport.title,
        description: vm.newReport.description,
        latitude: vm.newReport.latitude,
        longitude: vm.newReport.longitude
      };

      var postData = {
        report: report
      };

      var params = {
        user_id: $rootScope.currentUser.id
      };

      return ReportResourceFactory
        .save(
          params,
          postData,
          createSuccess,
          createError
        );

      function createSuccess(response, responseHeader) {
        $log.log('CreateReport Success');
        var reportId = response.report.id;
        if(vm.lastPhoto != null) {
          uploadImage(vm.lastPhoto, responseHeader().location, reportId);
        } else {
          vm.loading.report = false;
          $state.go('report-detail', { report_id: reportId });
        }
      }

      function createError(error) {
        vm.loading.report = false;
        AlertService.error("Could not create report. Please try again.");
        $log.error('CreateReport Error', error);
      }
    }

    function uploadImage(image, location, reportId) {
      var jpeg = true;
      var server = location + '/file/',
          targetPath = vm.lastPhoto,
          options = {
            fileKey: 'imageFile',
            httpMethod: 'POST',
            mimeType: jpeg ? 'image/jpg' : 'image/png',
            headers: {
              "Authorization": $rootScope.token
            }
          },
          trustAllHosts = true;

      $cordovaFileTransfer.upload(server, targetPath, options, trustAllHosts)
        .then(
          uploadImageSuccess,
          uploadImageError,
          uploadImageProgress
        )
      ;

      function uploadImageSuccess(response) {
        vm.loading.report = false;
        $state.go('report-detail', { report_id: reportId });
      }

      function uploadImageError(error) {
        vm.loading.report = false;
        AlertService.error("Could not upload Image. Please try again.");
        $log.error('uploadImage Error', error);
      }

      function uploadImageProgress(progress) {
        $timeout(function () {
          vm.uploadProgress = (progress.loaded / progress.total) * 100;
        });
      }
    }

    function prepareMap() {
      vm.tiles = {
        url: 'http://{s}.tile.openstreetmap.se/hydda/full/{z}/{x}/{y}.png'
      };
      angular.extend($scope, {
        mapCenter: {
          lat: vm.newReport.latitude,
          lng: vm.newReport.longitude,
          zoom: 15
        },
        markers: {
          currentPosMarker: {
            lat: vm.newReport.latitude,
            lng: vm.newReport.longitude,
            message: 'Current Location',
            focus: true,
            draggable: false
          }
        },
        defaults: {
          scrollWheelZoom: false
        }
      });
    }
  }

})();

;(function () {
  'use strict';

  ReportsResourceFactory.$inject = ['$resource', 'UriFactory'];
  angular.module('collect.reports')
    .factory('ReportsResourceFactory', ReportsResourceFactory);

  /* @ngInject */
  function ReportsResourceFactory(
    // Angular
    $resource,
    // Custom
    UriFactory
) {
    var url = UriFactory.getApi('reports/:report_id.:format');

    var paramDefaults = {
      report_id : '@id',
      format    : 'json'
    };

    return $resource(url, paramDefaults);
  }

})();

;(function () {
  'use strict';

  ReportResourceFactory.$inject = ['$resource', 'UriFactory'];
  angular.module('collect.reports')
    .factory('ReportResourceFactory', ReportResourceFactory);

  /* @ngInject */
  function ReportResourceFactory(
    // Angular
    $resource,
    // Custom
    UriFactory
) {
    var url = UriFactory.getApi('users/:user_id/reports.:format');

    var paramDefaults = {
      user_id : '@id',
      format  : 'json'
    };

    return $resource(url, paramDefaults);
  }

})();

;(function () {
  'use strict';

  ReportsCtrl.$inject = ['$log', '$state', '$scope', '$ionicLoading', 'config', 'ReportsResourceFactory'];
  angular.module('collect.reports')
    .controller('ReportsCtrl', ReportsCtrl);

  /* @ngInject */
  function ReportsCtrl(
    // Angular
    $log,
    $state,
    $scope,
    // Ionic
    $ionicLoading,
    // Custom
    config,
    ReportsResourceFactory
) {
    // ViewModel
    // =========
    var vm = this;

    vm.title = 'Reports';
    vm.platform = ionic.Platform.platform();
    vm.imagesUri = config.protocol + '://' + config.host + config.paths.images;
    vm.reports = getReports();
    vm.addReport = function() {
      $state.go('tabs.report-new');
    };
    vm.refreshReports = function() {
      vm.loading = false;
      vm.reports = getReports();
    }

    vm.loading = true;

    // Functions
    // =========

    // Reports
    // -------
    function getReports() {
      var params = null;

      return ReportsResourceFactory
        .query(
          params,
          getReportsSuccess,
          getReportsError
        );

      function getReportsSuccess(resource, responseHeader) {
        vm.loading = false;
        $scope.$broadcast('scroll.refreshComplete');
      }

      function getReportsError(error) {
        vm.loading = false;
        $scope.$broadcast('scroll.refreshComplete');
        $log.error('getReports Error:', error);
      }
    }
  }

})();

;(function () {
  'use strict';

  RegisterResourceFactory.$inject = ['$resource', 'UriFactory'];
  angular.module('collect.security')
    .factory('RegisterResourceFactory', RegisterResourceFactory);

  /* @ngInject */
  function RegisterResourceFactory(
    // Angular
    $resource,
    // Custom
    UriFactory
) {
    var url = UriFactory.getApi('register.:format');

    var paramDefaults = {
      format    : 'json'
    };

    var actions = {
      'create': {
        method: 'POST'
      }
    };

    return $resource(url, paramDefaults, actions);
  }

})();

;(function () {
  'use strict';

  SecurityCtrl.$inject = ['$log', '$scope', '$rootScope', '$state', '$ionicLoading', 'AlertService', 'AuthenticationService', 'RegisterResourceFactory', 'AccountResourceFactory'];
  angular.module('collect.security')
    .controller('SecurityCtrl', SecurityCtrl);

  /* @ngInject */
  function SecurityCtrl(
    // Angular
    $log,
    $scope,
    $rootScope,
    $state,
    // Ionic
    $ionicLoading,
    // Custom
    AlertService,
    AuthenticationService,
    RegisterResourceFactory,
    AccountResourceFactory
) {
    // ViewModel
    // =========
    var vm = this;

    // Functions
    // =========

    $scope.goToLoginPage = function() {
      $state.go('login');
    };
    $scope.goToRegisterPage = function() {
      $state.go('register');
    };

    $scope.submitLogin = function(credentials) {
      $ionicLoading.show({
        template: 'Authenticating',
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 200,
        showDelay: 0
      });
      AuthenticationService.login(credentials);
    };
    $scope.submitRegister = function(newUser) {
      if(newUser.plainPassword == newUser.passwordRepeat) {
        $ionicLoading.show({
          template: 'Submitting credentials',
          animation: 'fade-in',
          showBackdrop: true,
          maxWidth: 200,
          showDelay: 0
        });

        var user = {
          firstName: newUser.firstName,
          lastName: newUser.lastName,
          username: newUser.username,
          email: newUser.email,
          plainPassword: newUser.plainPassword
        };

        var params = {
          user: user
        };

        return RegisterResourceFactory
          .create(
            params,
            registerSuccess,
            registerError
          );

      } else {
        $scope.showAlert('Error', 'Passwords do not match!');
      }
    }

    function registerError(error) {
      $log.error('submitRegister Error:', error);
    }

    function registerSuccess(resource, responseHeader) {
      $ionicLoading.hide();
      AlertService.success(
        "Registration succesful. You can now log in.",
        function() {
          $state.go('welcome');
        }
      );
    }

    $scope.$on('event:auth-loginConfirmed', function(event, data){
      $ionicLoading.hide();
      $rootScope.isLoggedin = true;
      fetchUserDetails();
      $state.go('tabs.reports');
    });

    $scope.$on('event:auth-loginFailed', function(event, data){
      $ionicLoading.hide();
      $rootScope.isLoggedin = false;
      $log.error('Login failed', data);
      AlertService.error(data.message);
    });

    $scope.$on('event:auth-loginCancelled', function(event, data){
      $ionicLoading.hide();
      $rootScope.isLoggedin = false;
    });

    function fetchUserDetails() {
      var params = {
        'user': $rootScope.currentUserName
      };

      return AccountResourceFactory
        .get(
          params,
          success,
          error
        );

      function error(error) {
        $log.error('fetchUserDetails Error:', error);
      }

      function success(response) {
        $ionicLoading.hide();
        $rootScope.currentUser = response;
        return response;
      }
    }
  }

})();

;(function () {
  'use strict';

  AlertService.$inject = ['$log', '$ionicPopup'];
  angular.module('collect.services')
    .factory('AlertService', AlertService);

  /* @ngInject */
  function AlertService(
    $log,
    // Ionic
    $ionicPopup
) {
    return {
      success: function(msg, callback) {
        var successAlert = $ionicPopup.alert({
          title: 'Success!',
          template: msg
        });

        successAlert.then(callback);
      },
      error: function(msg, callback) {
        var errorAlert = $ionicPopup.alert({
          title: 'Error!',
          template: msg
        });

        errorAlert.then(callback);
      },
      confirm: function(title, msg, callback) {
        var confirmPopup = $ionicPopup.confirm({
          title: title,
          template: msg
        });

        confirmPopup.then(function(res) {
          if(res) {
            callback();
          }
        })
      }
    }
  }

})();

;(function () {
  'use strict';

  AuthenticationService.$inject = ['$rootScope', '$http', '$state', 'authService', '$httpBackend', '$location', '$ionicHistory', 'config'];
  angular.module('collect.services')
    .factory('AuthenticationService', AuthenticationService);

  /* @ngInject */
  function AuthenticationService(
    // Angular
    $rootScope,
    $http,
    $state,
    authService,
    $httpBackend,
    $location,
    // Ionic
    $ionicHistory,
    // Custom
    config
) {
    var uri = config.protocol + '://' + config.host + config.paths.api_login;

    return {
      login: function(credentials) {
        $http
          .post(uri, credentials, { ignoreAuthModule: true })
          .success(function(data, status, headers, config) {
            $http.defaults.headers.common.Authorization = 'Bearer ' + data.token;  // Step 1
            $rootScope.currentUserName = config.data.username;
            $rootScope.token = 'Bearer ' + data.token;
            authService.loginConfirmed(data, function(config) { // Step 2 + 3
              config.headers.Authorization = 'Bearer ' + data.token;
              return config;
            });
          })
          .error(function(data, status, headers, config) {
            $rootScope.$broadcast('event:auth-loginFailed', data);
          });
      },
      logout: function(user) {
        delete $http.defaults.headers.common.Authorization;
        $ionicHistory.nextViewOptions({
          disableBack: true
        });
        $state.go('welcome');
        $rootScope.$broadcast('event:auth-logoutComplete');
      }
    }
  }

})();

;(function () {
  'use strict';

  UriFactory.$inject = ['$location', 'config'];
  angular.module('collect.services')
    .factory('UriFactory', UriFactory);

  /* @ngInject */
  function UriFactory(
    // Angular
    $location,
    // Custom
    config
) {
    function getApi(path) {
      var protocol = config.protocol ? config.protocol : $location.protocol(),
          host     = config.host ? config.host : $location.host(),
          uri      = protocol + '://' + host + config.paths.api + path;

      return uri;
    }

    return {
      getApi: getApi
    };
  }

})();
