<?php

use Doctrine\Common\Annotations\AnnotationRegistry;
use Composer\Autoload\ClassLoader;

/**
 * @var ClassLoader
 */
// $loader = require __DIR__ . '/../vendor/autoload.php';
$loader = require '/home/vagrant/vendors/collect/autoload.php';

AnnotationRegistry::registerLoader(array($loader, 'loadClass'));

return $loader;
