<?php

namespace ApiBundle\Controller;

use AppBundle\Entity\Report;
use AppBundle\Entity\User;
use AppBundle\Entity\Status;
use ApiBundle\Form\Report\ReportType;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation as Nelmio;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ReportController extends FOSRestController
{
    /**
     * Test API options and requirements.
     *
     * @return Response
     *
     * @FOSRest\Options("/reports")
     *
     * @Nelmio\ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         Response::HTTP_OK: "OK"
     *     }
     * )
     */
    public function optionsReportsAction()
    {
        $response = new Response();
        $response->headers->set('Allow', 'OPTIONS, GET, POST, PUT, DELETE');

        return $response;
    }

    /**
     * Returns all reports.
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return mixed
     *
     * @FOSRest\View()
     * @FOSRest\Get(
     *     "/reports",
     *     requirements = {
     *         "_format" : "json|jsonp|xml"
     *     }
     * )
     * @FOSRest\QueryParam(
     *     name = "sort",
     *     requirements = "id|title",
     *     default = "id",
     *     description = "Order by Report id or Report title."
     * )
     * @FOSRest\QueryParam(
     *     name = "order",
     *     requirements = "asc|desc",
     *     default = "asc",
     *     description = "Order result ascending or descending."
     * )
     * @Nelmio\ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         Response::HTTP_OK : "OK"
     *     }
     * )
     */
    public function getReportsAction(ParamFetcher $paramFetcher)
    {
        # HTTP method: GET
        # Host/port  : http://www.collect.local
        #
        # Path       : /api/v1/reports.json
        # Path       : /api/v1/reports.xml
        # Path       : /api/v1/reports.xml?sort=title&amp;order=desc

        $em = $this->getDoctrine()->getManager();
        $reports = $em->getRepository(Report::class)->findAll();

        return $reports;
    }

    /**
     * Returns a report.
     *
     * @param $report_id
     *
     * @return object
     *
     * @FOSRest\View()
     * @FOSRest\Get(
     *     "/reports/{report_id}",
     *     requirements = {
     *         "report_id" : "\d+",
     *         "_format"   : "json|xml"
     *     }
     * )
     * @Nelmio\ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         Response::HTTP_OK        : "OK",
     *         Response::HTTP_NO_CONTENT: "No Content",
     *         Response::HTTP_NOT_FOUND : "Not Found"
     *     }
     * )
     */
    public function getReportAction($report_id)
    {
        # HTTP method: GET
        # Host/port  : http://www.collect.local
        #
        # Path       : /api/v1/users/1/reports/1.json

        $em = $this->getDoctrine()->getManager();

        $report = $em
            ->getRepository(Report::class)
            ->find($report_id);

        if (!$report instanceof Report) {
            throw new NotFoundHttpException('Report not found');
        }

        return $report;
    }

    /**
     * Returns all reports from a user.
     *
     * @param ParamFetcher $paramFetcher
     * @param $user_id
     *
     * @return mixed
     *
     * @FOSRest\View()
     * @FOSRest\Get(
     *     "/users/{user_id}/reports",
     *     requirements = {
     *         "_format" : "json|jsonp|xml"
     *     }
     * )
     * @FOSRest\QueryParam(
     *     name = "sort",
     *     requirements = "id|title",
     *     default = "id",
     *     description = "Order by Report id or Report title."
     * )
     * @FOSRest\QueryParam(
     *     name = "order",
     *     requirements = "asc|desc",
     *     default = "asc",
     *     description = "Order result ascending or descending."
     * )
     * @Nelmio\ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         Response::HTTP_OK : "OK"
     *     }
     * )
     */
    public function getUsersReportsAction(ParamFetcher $paramFetcher, $user_id)
    {
        # HTTP method: GET
        # Host/port  : http://www.collect.local
        #
        # Path       : /api/v1/users/1/reports.json
        # Path       : /api/v1/users/1/reports.xml
        # Path       : /api/v1/users/1/reports.xml?sort=title&amp;order=desc

        $em = $this->getDoctrine()->getManager();
        $user = $em
            ->getRepository(User::class)
            ->find($user_id);

        if (!$user instanceof User) {
            throw new NotFoundHttpException('User not found');
        }

        $reports = $user->getReports();

        return $reports;
    }

    /**
     * Post a new report.
     *
     * { "report": { "title": "Lorem", "description": "Ipsum", ... } }
     *
     * @param Request $request
     * @param $user_id
     *
     * @return View|Response
     *
     * @FOSRest\View()
     * @FOSRest\Post(
     *     "/users/{user_id}/reports",
     *     requirements = {
     *         "user_id": "\d+"
     *     }
     * )
     * @Nelmio\ApiDoc(
     *     input = ReportType::class,
     *     statusCodes = {
     *         Response::HTTP_CREATED : "Created"
     *     }
     * )
     */
    public function postCreateReportAction(Request $request, $user_id)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $em
            ->getRepository(User::class)
            ->find($user_id);
        if (!$user instanceof User) {
            throw new NotFoundHttpException('User not found');
        }

        $defaultStatus = $em->getRepository(Status::class)->findOneByName('Reported');
        $report = new Report($defaultStatus);
        $report->setUser($user);

        $points = $user->getPoints();
        $user->setPoints($points + 1);

        $logger = $this->get('logger');
        $logger->info($request);

        return $this->processCreateReportForm($request, $report, $user);
    }

    /**
     * Post an imageFile for the report.
     *
     * @param Request $request
     * @param $user_id
     * @param $report_id
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @FOSRest\View()
     * @FOSRest\Post(
     *     "/users/{user_id}/reports/{report_id}/file/",
     *     requirements = {
     *         "user_id"   : "\d+",
     *         "report_id" : "\d+",
     *     }
     * )
     */
    public function postReportImageAction(Request $request, $user_id, $report_id)
    {
        $em = $this->getDoctrine()->getManager();
        $report = $em->getRepository(Report::class)->find($report_id);

        $data = $_FILES['imageFile'];
        $file = new File($data['tmp_name'], $data['name'], $data['type'], $data['size'], $data['error']);

        $uploadDirectory = 'images/reports';
        $fileName = sha1_file($file->getRealPath()).'.'.$file->guessExtension();
        $fileLocator = realpath($this->getParameter('kernel.root_dir').DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'web').DIRECTORY_SEPARATOR.$uploadDirectory;
        $file->move($fileLocator, $fileName);
        $report->setImageFile($file);
        $report->setImageName($fileName);

        $em = $this->getDoctrine()->getManager();
        $em->persist($report);
        $em->flush();

        $response = new Response();

        return $response->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Update a report.
     *
     * @param Request $request
     * @param $user_id
     * @param $report_id
     *
     * @return Response
     *
     * @FOSRest\View()
     * @FOSRest\Put(
     *    "/users/{user_id}/reports/{report_id}",
     *     requirements = {
     *         "user_id" : "\d+",
     *         "report_id" : "\d+",
     *         "_format" : "json|xml"
     *     }
     * )
     * @Nelmio\ApiDoc(
     *     input = ReportType::class,
     *     statusCodes = {
     *         Response::HTTP_NO_CONTENT: "No Content"
     *     }
     * )
     */
    public function putEditReportAction(Request $request, $user_id, $report_id)
    {
        $em = $this->getDoctrine()->getManager();
        $report = $em
            ->getRepository(Report::class)
            ->find($report_id);

        if (!$report instanceof Report) {
            throw new NotFoundHttpException();
        }

        if ($report->getUser()->getId() === (int) $user_id) {
            return $this->processEditReportForm($request, $report);
        }
    }

    /**
     * Delete a report.
     *
     * @param $user_id
     * @param $report_id
     *
     * @throws NotFoundHttpException
     * @FOSRest\View(statusCode = 204)
     * @FOSRest\Delete(
     *     requirements = {
     *         "user_id"   : "\d+",
     *         "report_id" : "\d+",
     *         "_format"   : "json|xml"
     *     },
     *     defaults = {"_format": "json"}
     * )
     * @Nelmio\ApiDoc(
     *     statusCodes = {
     *         Response::HTTP_NO_CONTENT: "No Content",
     *         Response::HTTP_NOT_FOUND : "Not Found"
     *     }
     * )
     */
    public function deleteReportAction($user_id, $report_id)
    {
        $em = $this->getDoctrine()->getManager();

        $report = $em
            ->getRepository(Report::class)
            ->find($report_id);

        if (!$report instanceof Report) {
            throw new NotFoundHttpException('Report not found');
        }

        if ($report->getUser()->getId() === (int) $user_id) {
            $em->remove($report);
            $em->flush();
        }
    }

    // Convenience methods
    // -------------------

    /**
     * Process ReportType (Create) Form.
     *
     * @param Request $request
     * @param Report $report
     *
     * @return View|Response
     */
    private function processCreateReportForm(Request $request, Report $report, User $user)
    {
        $form = $this->createForm(ReportType::class, $report, ['method' => $request->getMethod()])->add('imageFile', 'file');
        $form->handleRequest($request);

        if ($form->isValid()) {
            $statusCode = is_null($report->getId()) ? Response::HTTP_CREATED : Response::HTTP_NO_CONTENT;

            $em = $this->getDoctrine()->getManager();
            $em->persist($report);  // Manage entity Report for persistence.
            $em->persist($user);    // Manage entity Report for persistence.
            $em->flush();           // Persist to database.

            $response = new Response();
            $response->setStatusCode($statusCode);

            // Redirect to the URI of the resource.
            $response->headers->set('Location',
                $this->generateUrl('api_v1_delete_user_report', [
                    'user_id' => $report->getUser()->getId(),
                    'report_id' => $report->getId(),
                ], /* absolute path = */true)
            );
            $response->setContent(json_encode([
                'report' => ['id' => $report->getId()],
            ]));

            return $response;
        }

        return View::create($form, Response::HTTP_BAD_REQUEST);
    }

    /**
     * Process ReportType (Edit) Form.
     *
     * @param Request $request
     * @param Report $report
     *
     * @return View|Response
     */
    private function processEditReportForm(Request $request, Report $report)
    {
        $form = $this->createForm(ReportType::class, $report, ['method' => $request->getMethod()])->add('imageFile', 'file');
        $form->handleRequest($request);

        if ($form->isValid()) {
            $statusCode = is_null($report->getId()) ? Response::HTTP_CREATED : Response::HTTP_NO_CONTENT;

            $report->setUpdatedAt(new \DateTime());

            $em = $this->getDoctrine()->getManager();
            $em->persist($report);  // Manage entity Report for persistence.
            $em->flush();           // Persist to database.

            $response = new Response();
            $response->setStatusCode($statusCode);

            // Redirect to the URI of the resource.
            $response->headers->set('Location',
                $this->generateUrl('api_v1_delete_user_report', [
                    'user_id' => $report->getUser()->getId(),
                    'report_id' => $report->getId(),
                ], /* absolute path = */true)
            );
            $response->setContent(json_encode([
                'report' => ['id' => $report->getId()],
            ]));

            return $response;
        }

        return View::create($form, Response::HTTP_BAD_REQUEST);
    }
}
