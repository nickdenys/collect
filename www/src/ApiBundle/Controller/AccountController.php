<?php

namespace ApiBundle\Controller;

use AppBundle\Entity\User;
use ApiBundle\Form\User\CreateType;
use ApiBundle\Form\User\EditType;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation as Nelmio;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AccountController extends FOSRestController
{
  /**
   * Test API options and requirements.
   *
   * @return Response
   *
   * @Nelmio\ApiDoc(
   *     resource = true,
   *     statusCodes = {
   *         Response::HTTP_OK: "OK"
   *     }
   * )
   */
  public function optionsRegisterAction()
  {
      $response = new Response();
      $response->headers->set('Allow', 'OPTIONS, POST');

      return $response;
  }

  /**
   * Register a new account.
   *
   * @param Request $request
   *
   * @return View|Response
   *
   * @FOSRest\View()
   * @FOSRest\Post(
   *     "/register"
   * )
   * @Nelmio\ApiDoc(
   *     input = CreateType::class,
   *     statusCodes = {
   *         Response::HTTP_CREATED : "Created"
   *     }
   * )
   */
  public function postRegisterAction(Request $request)
  {
    $user = new User();

    $logger = $this->get('logger');
    $logger->info($request);

    return $this->processCreateUserForm($request, $user);
  }

  // Convenience methods
  // -------------------

  /**
   * Process CreateType Form.
   *
   * @param Request $request
   * @param User $user
   *
   * @return View|Response
   */
  private function processCreateUserForm(Request $request, User $user)
  {
      $form = $this->createForm(CreateType::class, $user, ['method' => $request->getMethod()]);
      $form->handleRequest($request);

      if ($form->isValid()) {
          $statusCode = is_null($user->getId()) ? Response::HTTP_CREATED : Response::HTTP_NO_CONTENT;

          $em = $this->getDoctrine()->getManager();
          $em->persist($user);    // Manage entity for persistence.
          $em->flush();           // Persist to database.

          $response = new Response();
          $response->setStatusCode($statusCode);

          // Redirect to the URI of the resource.
          $response->headers->set('Location',
              $this->generateUrl('api_v1_get_user', [
                  'user' => $user->getId(),
              ], /* absolute path = */true)
          );
          $response->setContent(json_encode([
                  'user' => ['id' => $user->getId()],
              ]));

          return $response;
      }

      return View::create($form, Response::HTTP_BAD_REQUEST);
  }
}
