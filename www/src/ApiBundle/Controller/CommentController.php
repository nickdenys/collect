<?php

namespace ApiBundle\Controller;

use AppBundle\Entity\Report;
use AppBundle\Entity\User;
use AppBundle\Entity\Comment;
use ApiBundle\Form\Comment\CommentType;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation as Nelmio;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CommentController extends FOSRestController
{
    /**
     * Test API options and requirements.
     *
     * @return Response
     *
     * @Nelmio\ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         Response::HTTP_OK: "OK"
     *     }
     * )
     */
    public function optionsReportCommentsAction($report_id)
    {
        $response = new Response();
        $response->headers->set('Allow', 'OPTIONS, GET');

        return $response;
    }

    /**
     * Test API options and requirements.
     *
     * @return Response
     *
     * @Nelmio\ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         Response::HTTP_OK: "OK"
     *     }
     * )
     */
    public function optionsUserCommentsAction($user_id)
    {
        $response = new Response();
        $response->headers->set('Allow', 'OPTIONS, GET, POST, PUT');

        return $response;
    }

    /**
     * Returns all comments on a report.
     *
     * @param ParamFetcher $paramFetcher
     * @param $report_id
     *
     * @return mixed
     *
     * @FOSRest\View()
     * @FOSRest\Get(
     *     requirements = {
     *         "_format" : "json|jsonp|xml"
     *     }
     * )
     * @FOSRest\QueryParam(
     *     name = "sort",
     *     requirements = "(id|createdAt)",
     *     default = "createdAt",
     *     description = "Order by Comment id or Comment createdAt."
     * )
     * @FOSRest\QueryParam(
     *     name = "order",
     *     requirements = "(asc|desc)",
     *     default = "asc",
     *     description = "Order result ascending or descending."
     * )
     * @Nelmio\ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         Response::HTTP_OK : "OK"
     *     }
     * )
     */
    public function getReportCommentsAction(ParamFetcher $paramFetcher, $report_id)
    {
        # HTTP method: GET
        # Host/port  : http://www.collect.local
        #
        # Path       : /api/v1/reports/1/comments.json
        # Path       : /api/v1/reports/1/comments.xml
        # Path       : /api/v1/reports/1/comments.xml?sort=createdAt&amp;order=desc

        $em = $this->getDoctrine()->getManager();
        $report = $em
            ->getRepository(Report::class)
            ->find($report_id);

        if (!$report instanceof Report) {
            throw new NotFoundHttpException('User not found');
        }

        $comments = $em->getRepository(Comment::class)->getSortedComments($report->getId(), $paramFetcher->get('sort'), $paramFetcher->get('order'));

        return $comments;
    }

    /**
     * Post a new comment.
     *
     * @param Request $request
     * @param $user_id
     * @param $report_id
     *
     * @return View|Response
     *
     * @FOSRest\View()
     * @FOSRest\Post(
     *     "/users/{user_id}/comments/{report_id}",
     *     requirements = {
     *         "user_id": "\d+",
     *         "report_id": "\d+",
     *     }
     * )
     * @Nelmio\ApiDoc(
     *     input = CommentType::class,
     *     statusCodes = {
     *         Response::HTTP_CREATED : "Created"
     *     }
     * )
     */
    public function postReportCommentAction(Request $request, $user_id, $report_id)
    {
        $em = $this->getDoctrine()->getManager();

        $report = $em
            ->getRepository(Report::class)
            ->find($report_id);
        if (!$report instanceof Report) {
            throw new NotFoundHttpException('Report not found');
        }

        $user = $em
            ->getRepository(User::class)
            ->find($user_id);
        if (!$user instanceof User) {
            throw new NotFoundHttpException('User not found');
        }

        $comment = new Comment();
        $comment->setReport($report);
        $comment->setUser($user);

        $logger = $this->get('logger');
        $logger->info($request);

        return $this->processCommentForm($request, $comment);
    }

    /**
     * Update a comment.
     *
     * @param Request $request
     * @param $user_id
     * @param $comment_id
     *
     * @return Response
     *
     * @FOSRest\View()
     * @FOSRest\Put(
     *     "/users/{user_id}/comments/{comment_id}",
     *     requirements = {
     *         "user_id" : "\d+",
     *         "comment_id" : "\d+",
     *         "_format" : "json|xml"
     *     }
     * )
     * @Nelmio\ApiDoc(
     *     input = CommentType::class,
     *     statusCodes = {
     *         Response::HTTP_NO_CONTENT: "No Content"
     *     }
     * )
     */
    public function putCommentAction(Request $request, $user_id, $comment_id)
    {
        $em = $this->getDoctrine()->getManager();
        $comment = $em
            ->getRepository(Comment::class)
            ->find($comment_id);

        if (!$comment instanceof Comment) {
            throw new NotFoundHttpException('Comment not found');
        }

        if ($comment->getUser()->getId() === (int) $user_id) {
            return $this->processCommentForm($request, $comment, true);
        }
    }

    /**
     * Delete a comment.
     *
     * @param $user_id
     * @param $comment_id
     *
     * @throws NotFoundHttpException
     * @FOSRest\View(statusCode = 204)
     * @FOSRest\Delete(
     *     "/users/{user_id}/comments/{comment_id}",
     *     requirements = {
     *         "user_id"   : "\d+",
     *         "comment_id" : "\d+",
     *         "_format"   : "json|xml"
     *     },
     *     defaults = {"_format": "json"}
     * )
     * @Nelmio\ApiDoc(
     *     statusCodes = {
     *         Response::HTTP_NO_CONTENT: "No Content",
     *         Response::HTTP_NOT_FOUND : "Not Found"
     *     }
     * )
     */
    public function deleteCommentAction($user_id, $comment_id)
    {
        $em = $this->getDoctrine()->getManager();

        $comment = $em
            ->getRepository(Comment::class)
            ->find($comment_id);

        if (!$comment instanceof Comment) {
            throw new NotFoundHttpException('Comment not found');
        }

        if ($comment->getUser()->getId() === (int) $user_id) {
            $em->remove($comment);
            $em->flush();
        }
    }

    // Convenience methods
    // -------------------

    /**
     * Process CommentType Form.
     *
     * @param Request $request
     * @param Comment $comment
     *
     * @return View|Response
     */
    private function processCommentForm(Request $request, Comment $comment, $updated=false)
    {
        $form = $this->createForm(CommentType::class, $comment, ['method' => $request->getMethod()]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $statusCode = is_null($comment->getId()) ? Response::HTTP_CREATED : Response::HTTP_NO_CONTENT;

            if($updated) {
                $comment->setUpdatedAt(new \DateTime());
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($comment);  // Manage entity Report for persistence.
            $em->flush();            // Persist to database.

            $response = new Response();
            $response->setStatusCode($statusCode);

            // Redirect to the URI of the resource.
            $response->headers->set('Location',
                $this->generateUrl('api_v1_get_user_report', [
                    'report_id' => $comment->getReport()->getId(),
                ], /* absolute path = */true)
            );
            $response->setContent(json_encode([
                    'comment' => ['id' => $comment->getId()],
                ]));

            return $response;
        }

        return View::create($form, Response::HTTP_BAD_REQUEST);
    }
}
