<?php

namespace ApiBundle\Controller;

use AppBundle\Entity\User;
use ApiBundle\Form\User\EditType;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation as Nelmio;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UserController extends FOSRestController
{
    /**
     * Test API options and requirements.
     *
     * @return Response
     *
     * @Nelmio\ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         Response::HTTP_OK: "OK"
     *     }
     * )
     */
    public function optionsUsersAction()
    {
        $response = new Response();
        $response->headers->set('Allow', 'OPTIONS, GET, DELETE, PUT');

        return $response;
    }

    /**
     * Returns all users.
     *
     * @return mixed
     *
     * @FOSRest\View()
     * @FOSRest\Get(
     *     "/users",
     *     requirements = {
     *         "_format" : "json|jsonp|xml"
     *     }
     * )
     *
     * @Nelmio\ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         Response::HTTP_OK: "OK"
     *     }
     * )
     */
    public function getUsersAction()
    {
        $em = $this->getDoctrine()->getManager();
        $users = $em
            ->getRepository(User::class)
            ->findAll();

        return $users;
    }

    /**
     * Returns a user
     *
     * @param user_id
     *
     * @return mixed
     *
     * @FOSRest\View()
     * @FOSRest\Get(
     *     "/users/{user}",
     *     requirements = {
     *         "_format" : "json|jsonp|xml"
     *     }
     * )
     *
     * @Nelmio\ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         Response::HTTP_OK: "OK"
     *     }
     * )
     */
    public function getUserAction($user)
    {
        if (is_numeric($user)) {
          $em = $this->getDoctrine()->getManager();
          $user = $em
              ->getRepository(User::class)
              ->find($user);
        } else {
          $em = $this->getDoctrine()->getManager();
          $user = $em
              ->getRepository(User::class)
              ->findOneBy(array('username' => $user));
        }

        if (!$user instanceof User) {
            throw new NotFoundHttpException('User not found');
        }

        return $user;
    }

    /**
     * Update a user.
     *
     * @param Request $request
     * @param $user_id
     *
     * @return Response
     *
     * @FOSRest\View()
     * @FOSRest\Put(
     *    "/users/{user_id}",
     *     requirements = {
     *         "user_id" : "\d+",
     *         "_format" : "json|xml"
     *     }
     * )
     * @Nelmio\ApiDoc(
     *     input = EditType::class,
     *     statusCodes = {
     *         Response::HTTP_NO_CONTENT: "No Content"
     *     }
     * )
     */
    public function putEditUserAction(Request $request, $user_id)
    {
      $em = $this->getDoctrine()->getManager();
      $user = $em
          ->getRepository(User::class)
          ->find($user_id);

      if (!$user instanceof User) {
          throw new NotFoundHttpException();
      }

      return $this->processEditUserForm($request, $user);
    }

    /**
     * Delete a User.
     *
     * @param $user_id
     *
     * @throws NotFoundHttpException
     * @FOSRest\View(statusCode = 204)
     * @FOSRest\Delete(
     *     requirements = {
     *         "user_id"   : "\d+",
     *         "_format"   : "json|xml"
     *     },
     *     defaults = {"_format": "json"}
     * )
     * @Nelmio\ApiDoc(
     *     statusCodes = {
     *         Response::HTTP_NO_CONTENT: "No Content",
     *         Response::HTTP_NOT_FOUND : "Not Found"
     *     }
     * )
     */
    public function deleteUserAction($user_id)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $em
            ->getRepository(User::class)
            ->find($user_id);

        if (!$user instanceof User) {
            throw new NotFoundHttpException();
        }

        $em->remove($user);
        $em->flush();
    }

    /**
     * Test API options and requirements.
     *
     * @return Response
     *
     * @Nelmio\ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         Response::HTTP_OK: "OK"
     *     }
     * )
     */
    public function optionsLeaderboardAction()
    {
        $response = new Response();
        $response->headers->set('Allow', 'OPTIONS, GET');

        return $response;
    }

    /**
     * Returns the leaderboard
     *
     * @return mixed
     *
     * @FOSRest\View()
     * @FOSRest\Get(
     *     "/leaderboard",
     *     requirements = {
     *         "_format" : "json|jsonp|xml"
     *     }
     * )
     *
     * @Nelmio\ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         Response::HTTP_OK: "OK"
     *     }
     * )
     */
    public function getLeaderboardAction()
    {
      $em = $this->getDoctrine()->getManager();

      $users = $em
          ->getRepository(User::class)
          ->findLeaderboard();

      return $users;
    }

    // Convenience methods
    // -------------------

    /**
     * Process EditType (Update) Form.
     *
     * @param Request $request
     * @param User $user
     *
     * @return View|Response
     */
    public function processEditUserForm(Request $request, User $user)
    {
      $form = $this->createForm(EditType::class, $user, ['method' => $request->getMethod()]);
      $form->handleRequest($request);

      if ($form->isValid()) {
          $statusCode = is_null($user->getId()) ? Response::HTTP_CREATED : Response::HTTP_NO_CONTENT;

          $user->setUpdatedAt(new \DateTime());

          $em = $this->getDoctrine()->getManager();
          $em->persist($user);  // Manage entity Report for persistence.
          $em->flush();         // Persist to database.

          $response = new Response();
          $response->setStatusCode($statusCode);

          // Redirect to the URI of the resource.
          $response->headers->set('Location',
              $this->generateUrl('api_v1_get_user', [
                  'user' => $user->getId(),
              ], /* absolute path = */true)
          );
          $response->setContent(json_encode([
              'user' => ['id' => $user->getId()],
          ]));

          return $response;
      }

      return View::create($form, Response::HTTP_BAD_REQUEST);
    }
}
