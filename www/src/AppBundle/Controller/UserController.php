<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\User as UserForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class UserController extends Controller
{
    /**
     * Lists all User entities.
     *
     * @Route("/users", name="users_index")
     * @Method("GET")
     * @Template("User/list.html.twig")
     *
     * @param Request $request
     */
    public function listAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $dql = 'SELECT e FROM AppBundle:User e';
        $query = $em->createQuery($dql);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1), /* page number */
            10 /* limit per page */
        );

        $softDeleteForms = array();
        $unSoftDeleteForms = array();
        foreach ($pagination as $entity) {
            $softDeleteForms[$entity->getId()] = $this->createSoftDeleteForm($entity->getId())->createView();
            $unSoftDeleteForms[$entity->getId()] = $this->createUnSoftDeleteForm($entity->getId())->createView();
        }

        return [
            'pagination' => $pagination,
            'softDeleteForms' => $softDeleteForms,
            'unSoftDeleteForms' => $unSoftDeleteForms,
        ];
    }

    /**
     * Lists all User entities that match search words.
     *
     * @Route("/users/search", name="users_search")
     * @Method("POST")
     * @Template("User/list.html.twig")
     *
     * @param Request $request
     */
    public function searchAction(Request $request)
    {
        if ($request->request->get('key')) {
            $q = $request->request->all(); // Get the posted data
            $key = $q['key']; // Get the search criteria

            if ($key === '' || $key === ' ') {
                $this->addFlash(
                    'warning',
                    'Please enter valid terms when searching.'
                );

                return $this->redirect($this->generateUrl('users_index'));
            }

            $em = $this->getDoctrine()->getManager();
            $repo = $em->getRepository('AppBundle:User');
            $query = $repo->createQueryBuilder('e')
                            ->where('e.username LIKE :key')
                            ->orWhere('e.emailCanonical LIKE :key')
                            ->orWhere('e.firstName LIKE :key')
                            ->orWhere('e.lastName LIKE :key')
                            ->setParameter('key', '%'.$key.'%')
                            ->getQuery();

            $paginator = $this->get('knp_paginator');
            $pagination = $paginator->paginate(
                $query,
                $request->query->getInt('page', 1),
                10
            );

            $softDeleteForms = array();
            $unSoftDeleteForms = array();
            foreach ($pagination as $entity) {
                $softDeleteForms[$entity->getId()] = $this->createSoftDeleteForm($entity->getId())->createView();
                $unSoftDeleteForms[$entity->getId()] = $this->createUnSoftDeleteForm($entity->getId())->createView();
            }

            if ($pagination->count() != 1) {
                $countMessage = 'Found '.$pagination->count().' results.';
            } else {
                $countMessage = 'Found 1 result.';
            }
            $this->addFlash(
                'info',
                $countMessage
            );

            return [
                'pagination' => $pagination,
                'softDeleteForms' => $softDeleteForms,
                'unSoftDeleteForms' => $unSoftDeleteForms,
                'key' => $key,
            ];
        } else {
            // If there's no POST-data, redirect to index
            return $this->redirect($this->generateUrl('users_index'));
        }
    }

    /**
     * Show details of one User entity.
     *
     * @Route("/user/{$id}", name="users_detail", requirements={"id" = "\d+"},)
     * @Method("GET")
     * @Template("User/detail.html.twig")
     *
     * @param $id
     */
    public function detailAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository(User::class)->findOneWithReportsAndComments($id);

        if (!$entity) {
            $this->addFlash(
                'error',
                'Something went wrong! [Unable to find User entity]'
            );

            return $this->redirect($this->generateUrl('users_index'));
        }

        $softDeleteForm = $this->createSoftDeleteForm($entity->getId())->createView();
        $unSoftDeleteForm = $this->createUnSoftDeleteForm($entity->getId())->createView();
        $deleteForm = $this->createDeleteForm($entity->getId())->createView();

        return [
            'user' => $entity,
            'softDeleteForm' => $softDeleteForm,
            'unSoftDeleteForm' => $unSoftDeleteForm,
            'deleteForm' => $deleteForm,
        ];
    }

    /**
     * Creates a form to edit a User entity.
     *
     * @param $id
     *
     * @return \Symfony\Component\Form\Form
     */
    private function createEditForm(User $entity, $id)
    {
        $form = $this->createForm(UserForm\EditType::class, $entity, [
            'action' => $this->generateUrl('users_update', array('id' => $id)),
            'method' => Request::METHOD_POST,
        ]);

        return $form;
    }

    /**
     * Displays a form to edit an existing User entity.
     *
     * @Route("user/{id}/edit", name="users_edit", requirements={"id": "\d+"})
     * @Method("GET")
     * @Template("User/edit.html.twig")
     *
     * @param $id
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository(User::class)->find($id);

        if (!$entity) {
            $this->addFlash(
                'error',
                'Something went wrong! [Unable to find User entity]'
            );

            return $this->redirect($this->generateUrl('users_index'));
        }

        $editForm = $this->createEditForm($entity, $id);
        $deleteForm = $this->createDeleteForm($id);

        return [
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ];
    }

    /**
     * Edits an existing User entity.
     *
     * @Route("/user/{id}/edit", name="users_update", requirements={"id": "\d+"})
     * @Method("POST")
     * @Template("User/edit.html.twig")
     *
     * @param Request $request
     * @param $id
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository(User::class)->find($id);

        if (!$entity) {
            $this->addFlash(
                'error',
                'Something went wrong! [Unable to find User entity]'
            );

            return $this->redirect($this->generateUrl('users_index'));
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity, $id);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $entity->setUpdatedAt(new \DateTime());
            $em->persist($entity);
            $em->flush();

            $this->addFlash(
                'success',
                'User updated!'
            );

            return $this->redirect($this->generateUrl('users_detail', ['id' => $id]));
        }

        return [
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ];
    }

    /**
     * Creates a form to softdelete a User entity by id.
     *
     * This is necessary because browsers don't support HTTP methods different
     * from GET and POST. Since the controller expects
     * a DELETE method, the trick is to create a simple form that *fakes* the
     * HTTP DELETE method.
     * See http://symfony.com/doc/current/cookbook/routing/method_parameters.html.
     *
     * @param $id
     *
     * @return \Symfony\Component\Form\Form
     */
    private function createSoftDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('users_softdelete', ['id' => $id]))
            ->setMethod('POST')
            ->getForm()
        ;
    }

    /**
     * Softdeletes a User entity.
     *
     * @Route("/users/softdelete/{id}", requirements={"id" = "\d+"}, name="users_softdelete")
     * @Method("POST")
     *
     * @param Request $request
     * @param $id
     *
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function softDeleteAction(Request $request, $id)
    {
        $form = $this->createSoftDeleteForm($id);
        $form->handleRequest($request);

        $previousUrl = $request->headers->get('referer');

        $currentUser = $this->get('security.token_storage')->getToken()->getUser()->getId();
        if ($currentUser == $id) {
            $this->addFlash(
                'error',
                'You can\'t disable your own account!'
            );

            if (isset($previousUrl)) {
                return $this->redirect($previousUrl);
            } else {
                return $this->redirect($this->generateUrl('users_index'));
            }
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository(User::class)->find($id);
            $entity->setDeletedAt(new \DateTime('now'));
            $entity->setEnabled(false);
            $em->flush();

            $this->addFlash(
                'success',
                'User disabled!'
            );
        }

        if (isset($previousUrl)) {
            return $this->redirect($previousUrl);
        } else {
            return $this->redirect($this->generateUrl('users_index'));
        }
    }

    /**
     * Creates a form to unsoftdelete a User entity by id.
     *
     * This is necessary because browsers don't support HTTP methods different
     * from GET and POST. Since the controller expects
     * a DELETE method, the trick is to create a simple form that *fakes* the
     * HTTP DELETE method.
     * See http://symfony.com/doc/current/cookbook/routing/method_parameters.html.
     *
     * @param $id
     *
     * @return \Symfony\Component\Form\Form
     */
    private function createUnSoftDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('users_unsoftdelete', ['id' => $id]))
            ->setMethod('POST')
            ->getForm()
        ;
    }

    /**
     * Unsoftdeletes a User entity.
     *
     * @Route("/users/unsoftdelete/{id}", requirements={"id" = "\d+"}, name="users_unsoftdelete")
     * @Method("POST")
     *
     * @param Request $request
     * @param $id
     *
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function unSoftDeleteAction(Request $request, $id)
    {
        $form = $this->createUnSoftDeleteForm($id);
        $form->handleRequest($request);

        $previousUrl = $request->headers->get('referer');

        $currentUser = $this->get('security.token_storage')->getToken()->getUser()->getId();
        if ($currentUser == $id) {
            $this->addFlash(
                'error',
                'You can\'t enable your own account!'
            );

            if (isset($previousUrl)) {
                return $this->redirect($previousUrl);
            } else {
                return $this->redirect($this->generateUrl('users_index'));
            }
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository(User::class)->find($id);
            $entity->setDeletedAt(null);
            $entity->setEnabled(true);
            $em->flush();

            $this->addFlash(
                'success',
                'User enabled!'
            );
        }

        if (isset($previousUrl)) {
            return $this->redirect($previousUrl);
        } else {
            return $this->redirect($this->generateUrl('users_index'));
        }
    }

    /**
     * Creates a form to delete a User entity by id.
     *
     * This is necessary because browsers don't support HTTP methods different
     * from GET and POST. Since the controller expects
     * a DELETE method, the trick is to create a simple form that *fakes* the
     * HTTP DELETE method.
     * See http://symfony.com/doc/current/cookbook/routing/method_parameters.html.
     *
     * @param $id
     *
     * @return \Symfony\Component\Form\Form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('users_delete', ['id' => $id]))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
     * Deletes a User entity.
     *
     * @Route("/users/delete/{id}", requirements={"id" = "\d+"}, name="users_delete")
     * @Method("DELETE")
     *
     * @param Request $request
     * @param $id
     *
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        $currentUser = $this->get('security.token_storage')->getToken()->getUser()->getId();
        if ($currentUser == $id) {
            $this->addFlash(
                'error',
                'You can\'t delete your own account!'
            );

            return $this->redirect($this->generateUrl('users_index'));
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository(User::class)->find($id);
            $em->remove($entity);
            $em->flush();

            $this->addFlash(
                'success',
                'User deleted!'
            );
        }

        return $this->redirect($this->generateUrl('users_index'));
    }
}
