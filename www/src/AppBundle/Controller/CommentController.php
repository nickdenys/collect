<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Comment;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class CommentController extends Controller
{
    /**
     * Lists all Comment entities.
     *
     * @Route("/comments", name="comments_index")
     * @Method("GET")
     * @Template("Comment/list.html.twig")
     *
     * @param Request $request
     */
    public function listAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $dql = 'SELECT e FROM AppBundle:Comment e';
        $query = $em->createQuery($dql);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1), /* page number */
            5 /* limit per page */
        );

        $softDeleteForms = array();
        $unSoftDeleteForms = array();
        foreach ($pagination as $entity) {
            $softDeleteForms[$entity->getId()] = $this->createSoftDeleteForm($entity->getId())->createView();
            $unSoftDeleteForms[$entity->getId()] = $this->createUnSoftDeleteForm($entity->getId())->createView();
        }

        return [
            'pagination' => $pagination,
            'softDeleteForms' => $softDeleteForms,
            'unSoftDeleteForms' => $unSoftDeleteForms,
        ];
    }

    /**
     * Lists all Comment entities that match search words.
     *
     * @Route("/comments/search", name="comments_search")
     * @Method("POST")
     * @Template("Comment/list.html.twig")
     *
     * @param Request $request
     */
    public function searchAction(Request $request)
    {
        if ($request->request->get('key')) {
            $q = $request->request->all(); // Get the posted data
            $key = $q['key']; // Get the search criteria

            if ($key === '' || $key === ' ') {
                $this->addFlash(
                    'warning',
                    'Please enter valid terms when searching.'
                );

                return $this->redirect($this->generateUrl('comments_index'));
            }

            $em = $this->getDoctrine()->getManager();
            $repo = $em->getRepository('AppBundle:Comment');
            $query = $repo->createQueryBuilder('e')
                            ->where('e.content LIKE :key')
                            ->setParameter('key', '%'.$key.'%')
                            ->getQuery();

            $paginator = $this->get('knp_paginator');
            $pagination = $paginator->paginate(
                $query,
                $request->query->getInt('page', 1),
                10
            );

            $softDeleteForms = array();
            $unSoftDeleteForms = array();
            foreach ($pagination as $entity) {
                $softDeleteForms[$entity->getId()] = $this->createSoftDeleteForm($entity->getId())->createView();
                $unSoftDeleteForms[$entity->getId()] = $this->createUnSoftDeleteForm($entity->getId())->createView();
            }

            if ($pagination->count() != 1) {
                $countMessage = 'Found '.$pagination->count().' results.';
            } else {
                $countMessage = 'Found 1 result.';
            }
            $this->addFlash(
                'info',
                $countMessage
            );

            return [
                'pagination' => $pagination,
                'softDeleteForms' => $softDeleteForms,
                'unSoftDeleteForms' => $unSoftDeleteForms,
                'key' => $key,
            ];
        } else {
            // If there's no POST-data, redirect to index
            return $this->redirect($this->generateUrl('comments_index'));
        }
    }

    /**
     * Show details of one Comment entity.
     *
     * @Route("/comment/{$id}", name="comments_detail", requirements={"id" = "\d+"},)
     * @Method("GET")
     * @Template("Comment/detail.html.twig")
     *
     * @param $id
     */
    public function detailAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository(Comment::class)->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Comment entity.');

            $this->addFlash(
                'error',
                'Something went wrong! [Unable to find Comment entity]'
            );
        }

        $softDeleteForm = $this->createSoftDeleteForm($entity->getId())->createView();
        $unSoftDeleteForm = $this->createUnSoftDeleteForm($entity->getId())->createView();
        $deleteForm = $this->createDeleteForm($entity->getId())->createView();

        return [
            'comment' => $entity,
            'softDeleteForm' => $softDeleteForm,
            'unSoftDeleteForm' => $unSoftDeleteForm,
            'deleteForm' => $deleteForm,
        ];
    }

    /**
     * Creates a form to softdelete a Comment entity by id.
     *
     * This is necessary because browsers don't support HTTP methods different
     * from GET and POST. Since the controller expects
     * a DELETE method, the trick is to create a simple form that *fakes* the
     * HTTP DELETE method.
     * See http://symfony.com/doc/current/cookbook/routing/method_parameters.html.
     *
     * @param $id
     *
     * @return \Symfony\Component\Form\Form
     */
    private function createSoftDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('comments_softdelete', ['id' => $id]))
            ->setMethod('POST')
            ->getForm()
        ;
    }

    /**
     * Softdeletes a Comment entity.
     *
     * @Route("/comments/softdelete/{id}", requirements={"id" = "\d+"}, name="comments_softdelete")
     * @Method("POST")
     *
     * @param Request $request
     * @param $id
     *
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function softDeleteAction(Request $request, $id)
    {
        $form = $this->createSoftDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository(Comment::class)->find($id);
            $entity->setDeletedAt(new \DateTime('now'));
            $em->flush();

            $this->addFlash(
                'success',
                'Comment disabled!'
            );
        }

        $previousUrl = $request->headers->get('referer');
        if (isset($previousUrl)) {
            return $this->redirect($previousUrl);
        } else {
            return $this->redirect($this->generateUrl('comments_index'));
        }
    }

    /**
     * Creates a form to unsoftdelete a Comment entity by id.
     *
     * This is necessary because browsers don't support HTTP methods different
     * from GET and POST. Since the controller expects
     * a DELETE method, the trick is to create a simple form that *fakes* the
     * HTTP DELETE method.
     * See http://symfony.com/doc/current/cookbook/routing/method_parameters.html.
     *
     * @param $id
     *
     * @return \Symfony\Component\Form\Form
     */
    private function createUnSoftDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('comments_unsoftdelete', ['id' => $id]))
            ->setMethod('POST')
            ->getForm()
        ;
    }

    /**
     * Unsoftdeletes a Comment entity.
     *
     * @Route("/comments/unsoftdelete/{id}", requirements={"id" = "\d+"}, name="comments_unsoftdelete")
     * @Method("POST")
     *
     * @param Request $request
     * @param $id
     *
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function unSoftDeleteAction(Request $request, $id)
    {
        $form = $this->createUnSoftDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository(Comment::class)->find($id);
            $entity->setDeletedAt(null);
            $em->flush();

            $this->addFlash(
                'success',
                'Comment enabled!'
            );
        }

        $previousUrl = $request->headers->get('referer');
        if (isset($previousUrl)) {
            return $this->redirect($previousUrl);
        } else {
            return $this->redirect($this->generateUrl('comments_index'));
        }
    }

    /**
     * Creates a form to delete a Comment entity by id.
     *
     * This is necessary because browsers don't support HTTP methods different
     * from GET and POST. Since the controller expects
     * a DELETE method, the trick is to create a simple form that *fakes* the
     * HTTP DELETE method.
     * See http://symfony.com/doc/current/cookbook/routing/method_parameters.html.
     *
     * @param $id
     *
     * @return \Symfony\Component\Form\Form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('comments_delete', ['id' => $id]))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
     * Deletes a Comment entity.
     *
     * @Route("/comments/delete/{id}", requirements={"id" = "\d+"}, name="comments_delete")
     * @Method("DELETE")
     *
     * @param Request $request
     * @param $id
     *
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository(Comment::class)->find($id);
            $em->remove($entity);
            $em->flush();

            $this->addFlash(
                'success',
                'Comment deleted!'
            );
        }

        return $this->redirect($this->generateUrl('comments_index'));
    }
}
