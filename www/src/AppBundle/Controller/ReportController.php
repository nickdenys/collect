<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Report;
use AppBundle\Entity\User;
use AppBundle\Form\Report as ReportForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ReportController extends Controller
{
    /**
     * Lists all Report entities.
     *
     * @Route("/reports", name="reports_index")
     * @Method("GET")
     * @Template("Report/list.html.twig")
     *
     * @param Request $request
     */
    public function listAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $dql = 'SELECT e FROM AppBundle:Report e';
        $query = $em->createQuery($dql);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1), /* page number */
            10 /* limit per page */
        );

        $softDeleteForms = array();
        $unSoftDeleteForms = array();
        foreach ($pagination as $entity) {
            $softDeleteForms[$entity->getId()] = $this->createSoftDeleteForm($entity->getId())->createView();
            $unSoftDeleteForms[$entity->getId()] = $this->createUnSoftDeleteForm($entity->getId())->createView();
        }

        return [
            'pagination' => $pagination,
            'softDeleteForms' => $softDeleteForms,
            'unSoftDeleteForms' => $unSoftDeleteForms,
        ];
    }

    /**
     * Lists all Report entities that match search words.
     *
     * @Route("/reports/search", name="reports_search")
     * @Method("POST")
     * @Template("Report/list.html.twig")
     *
     * @param Request $request
     */
    public function searchAction(Request $request)
    {
        if ($request->request->get('key')) {
            $q = $request->request->all(); // Get the posted data
            $key = $q['key']; // Get the search criteria

            if ($key === '' || $key === ' ') {
                $this->addFlash(
                    'warning',
                    'Please enter valid terms when searching.'
                );

                return $this->redirect($this->generateUrl('reports_index'));
            }

            $em = $this->getDoctrine()->getManager();
            $repo = $em->getRepository('AppBundle:Report');
            $query = $repo->createQueryBuilder('e')
                            ->where('e.title LIKE :key')
                            ->orWhere('e.description LIKE :key')
                            ->setParameter('key', '%'.$key.'%')
                            ->getQuery();

            $paginator = $this->get('knp_paginator');
            $pagination = $paginator->paginate(
                $query,
                $request->query->getInt('page', 1),
                10
            );

            $softDeleteForms = array();
            $unSoftDeleteForms = array();
            foreach ($pagination as $entity) {
                $softDeleteForms[$entity->getId()] = $this->createSoftDeleteForm($entity->getId())->createView();
                $unSoftDeleteForms[$entity->getId()] = $this->createUnSoftDeleteForm($entity->getId())->createView();
            }

            if ($pagination->count() != 1) {
                $countMessage = 'Found '.$pagination->count().' results.';
            } else {
                $countMessage = 'Found 1 result.';
            }
            $this->addFlash(
                'info',
                $countMessage
            );

            return [
                'pagination' => $pagination,
                'softDeleteForms' => $softDeleteForms,
                'unSoftDeleteForms' => $unSoftDeleteForms,
                'key' => $key,
            ];
        } else {
            // If there's no POST-data, redirect to index
            return $this->redirect($this->generateUrl('reports_index'));
        }
    }

    /**
     * Show details of one Report entity.
     *
     * @Route("/report/{$id}", name="reports_detail", requirements={"id" = "\d+"},)
     * @Method("GET")
     * @Template("Report/detail.html.twig")
     *
     * @param $id
     */
    public function detailAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository(Report::class)->findOneWithComments($id);

        if (!$entity) {
            $this->addFlash(
                'error',
                'Something went wrong! [Unable to find Report entity]'
            );

            return $this->redirect($this->generateUrl('reports_index'));
        }

        $softDeleteForm = $this->createSoftDeleteForm($entity->getId())->createView();
        $unSoftDeleteForm = $this->createUnSoftDeleteForm($entity->getId())->createView();
        $deleteForm = $this->createDeleteForm($entity->getId())->createView();

        return [
            'report' => $entity,
            'softDeleteForm' => $softDeleteForm,
            'unSoftDeleteForm' => $unSoftDeleteForm,
            'deleteForm' => $deleteForm,
        ];
    }

    /**
     * Creates a form to create a Report entity.
     *
     * @param Report $entity
     *
     * @return \Symfony\Component\Form\Form
     */
    private function createCreateForm(Report $entity)
    {
        $form = $this->createForm(ReportForm\NewType::class, $entity, [
            'action' => $this->generateUrl('reports_create'),
            'method' => Request::METHOD_POST,
        ]);

        return $form;
    }

    /**
     * Displays a form to create a new Report entity.
     *
     * @Route("/reports/new", name="reports_new")
     * @Method("GET")
     * @Template("Report/new.html.twig")
     */
    public function newAction()
    {
        $entity = new Report();
        $newForm = $this->createCreateForm($entity);

        // Send variables to the view.
        return [
            'new_form' => $newForm->createView(),
        ];
    }

    /**
     * Creates a new Report entity.
     *
     * @Route("/reports/new", name="reports_create")
     * @Method("POST")
     * @Template("Report/new.html.twig")
     *
     * @param Request $request
     *
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function createAction(Request $request)
    {
        $entity = new Report();
        $entity->setUser($this->getUser());
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->addFlash(
                'success',
                'Report added!'
            );

            return $this->redirect($this->generateUrl('reports_index'));
        } else {
            $this->addFlash(
                'error',
                'Something went wrong!'
            );
        }

        // Send variables to the view.
        return [
            'report' => $entity,
            'new_form' => $form->createView(),
        ];
    }

    /**
     * Creates a form to edit a Report entity.
     *
     * @param $id
     *
     * @return \Symfony\Component\Form\Form
     */
    private function createEditForm(Report $entity, $id)
    {
        $form = $this->createForm(ReportForm\EditType::class, $entity, [
            'action' => $this->generateUrl('reports_update', array('id' => $id)),
            'method' => Request::METHOD_POST,
        ]);

        return $form;
    }

    /**
     * Displays a form to edit an existing Report entity.
     *
     * @Route("report/{id}/edit", name="reports_edit", requirements={"id": "\d+"})
     * @Method("GET")
     * @Template("Report/edit.html.twig")
     *
     * @param $id
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository(Report::class)->find($id);

        if (!$entity) {
            $this->addFlash(
                'error',
                'Something went wrong! [Unable to find Report entity]'
            );

            return $this->redirect($this->generateUrl('reports_index'));
        }

        $editForm = $this->createEditForm($entity, $id);
        $deleteForm = $this->createDeleteForm($id);

        return [
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ];
    }

    /**
     * Edits an existing Report entity.
     *
     * @Route("/report/{id}/edit", name="reports_update", requirements={"id": "\d+"})
     * @Method("POST")
     * @Template("Report/edit.html.twig")
     *
     * @param Request $request
     * @param $id
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository(Report::class)->find($id);

        if (!$entity) {
            $this->addFlash(
                'error',
                'Something went wrong! [Unable to find Report entity]'
            );

            return $this->redirect($this->generateUrl('reports_index'));
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity, $id);
        $oldStatus = $entity->getStatus()->getName();
        $editForm->handleRequest($request);
        $newStatus = $entity->getStatus()->getName();

        if ($editForm->isValid()) {
            $entity->setUpdatedAt(new \DateTime());

            // Give points to user when status updates
            if ($oldStatus != $newStatus) {
              $user = $entity->getUser();
              $points = $user->getPoints();

              switch ($newStatus) {
                case 'Confirmed':
                  $user->setPoints($points + 20);
                  break;
                case 'Fixed':
                  $user->setPoints($points + 10);
                  break;
                case 'Closed':
                  $user->setPoints($points + 5);
                  break;
              }

              $em->persist($user);
            }

            $em->persist($entity);
            $em->flush();

            $this->addFlash(
                'success',
                'Report updated!'
            );

            return $this->redirect($this->generateUrl('reports_detail', ['id' => $id]));
        } else {
            $this->addFlash(
                'error',
                'Something went wrong!'
            );
        }

        return [
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ];
    }

    /**
     * Creates a form to softdelete a Report entity by id.
     *
     * This is necessary because browsers don't support HTTP methods different
     * from GET and POST. Since the controller expects
     * a DELETE method, the trick is to create a simple form that *fakes* the
     * HTTP DELETE method.
     * See http://symfony.com/doc/current/cookbook/routing/method_parameters.html.
     *
     * @param $id
     *
     * @return \Symfony\Component\Form\Form
     */
    private function createSoftDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('reports_softdelete', ['id' => $id]))
            ->setMethod('POST')
            ->getForm()
        ;
    }

    /**
     * Softdeletes a Report entity.
     *
     * @Route("/reports/softdelete/{id}", requirements={"id" = "\d+"}, name="reports_softdelete")
     * @Method("POST")
     *
     * @param Request $request
     * @param $id
     *
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function softDeleteAction(Request $request, $id)
    {
        $form = $this->createSoftDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository(Report::class)->find($id);
            $entity->setDeletedAt(new \DateTime('now'));
            $em->flush();

            $this->addFlash(
                'success',
                'Report disabled!'
            );
        } else {
            $this->addFlash(
                'error',
                'Something went wrong!'
            );
        }

        $previousUrl = $request->headers->get('referer');
        if (isset($previousUrl)) {
            return $this->redirect($previousUrl);
        } else {
            return $this->redirect($this->generateUrl('reports_index'));
        }
    }

    /**
     * Creates a form to unsoftdelete a Report entity by id.
     *
     * This is necessary because browsers don't support HTTP methods different
     * from GET and POST. Since the controller expects
     * a DELETE method, the trick is to create a simple form that *fakes* the
     * HTTP DELETE method.
     * See http://symfony.com/doc/current/cookbook/routing/method_parameters.html.
     *
     * @param $id
     *
     * @return \Symfony\Component\Form\Form
     */
    private function createUnSoftDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('reports_unsoftdelete', ['id' => $id]))
            ->setMethod('POST')
            ->getForm()
        ;
    }

    /**
     * Unsoftdeletes a Report entity.
     *
     * @Route("/reports/unsoftdelete/{id}", requirements={"id" = "\d+"}, name="reports_unsoftdelete")
     * @Method("POST")
     *
     * @param Request $request
     * @param $id
     *
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function unSoftDeleteAction(Request $request, $id)
    {
        $form = $this->createUnSoftDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository(Report::class)->find($id);
            $entity->setDeletedAt(null);
            $em->flush();

            $this->addFlash(
                'success',
                'Report enabled!'
            );
        } else {
            $this->addFlash(
                'error',
                'Something went wrong!'
            );
        }

        $previousUrl = $request->headers->get('referer');
        if (isset($previousUrl)) {
            return $this->redirect($previousUrl);
        } else {
            return $this->redirect($this->generateUrl('reports_index'));
        }
    }

    /**
     * Creates a form to delete a Report entity by id.
     *
     * This is necessary because browsers don't support HTTP methods different
     * from GET and POST. Since the controller expects
     * a DELETE method, the trick is to create a simple form that *fakes* the
     * HTTP DELETE method.
     * See http://symfony.com/doc/current/cookbook/routing/method_parameters.html.
     *
     * @param $id
     *
     * @return \Symfony\Component\Form\Form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('reports_delete', ['id' => $id]))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
     * Deletes a Report entity.
     *
     * @Route("/reports/delete/{id}", requirements={"id" = "\d+"}, name="reports_delete")
     * @Method("DELETE")
     *
     * @param Request $request
     * @param $id
     *
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository(Report::class)->find($id);
            $em->remove($entity);
            $em->flush();

            $this->addFlash(
                'success',
                'Report deleted!'
            );
        } else {
            $this->addFlash(
                'error',
                'Something went wrong!'
            );
        }

        return $this->redirect($this->generateUrl('reports_index'));
    }
}
