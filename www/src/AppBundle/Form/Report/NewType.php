<?php

namespace AppBundle\Form\Report;

use AppBundle\Entity\Report;
use AppBundle\Entity\Status;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType as VichImage;

class NewType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('description')
            ->add('latitude')
            ->add('longitude')
            ->add('imageFile', VichImage::class, array(
                'required' => true,
                'allow_delete' => true,
                'download_link' => false,
                'attr' => array('class' => 'validate', 'accept' => 'image/*'),
            ))
            ->add('status', EntityType::class, [
                'class' => Status::class,
                'choice_label' => 'name',
                'expanded' => false,
                'multiple' => false,
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Report::class,
        ));
    }

    /**
     * Form name.
     *
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'appbundle_report_new';
    }
}
