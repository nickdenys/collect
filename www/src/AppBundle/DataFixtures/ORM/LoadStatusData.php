<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Status;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadStatusData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 1; // The order in which fixture(s) will be loaded.
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $em)
    {
        $status = new Status();
        $em->persist($status);
        $status->setName('Reported');
        $status->setDescription('This report has been added, but is currently pending for review.');
        $this->addReference('Status-Reported', $status); // Reference for the next Data Fixture(s).

        $status = new Status();
        $em->persist($status);
        $status->setName('Confirmed');
        $status->setDescription('This report has been confirmed and is in queue for appropriate treatment.');
        $this->addReference('Status-Confirmed', $status); // Reference for the next Data Fixture(s).

        $status = new Status();
        $em->persist($status);
        $status->setName('Fixed');
        $status->setDescription('This report has been treated and is awaiting confirmation.');
        $this->addReference('Status-Fixed', $status); // Reference for the next Data Fixture(s).

        $status = new Status();
        $em->persist($status);
        $status->setName('Closed');
        $status->setDescription('This report has been confirmed and closed.');
        $this->addReference('Status-Closed', $status); // Reference for the next Data Fixture(s).

        $em->flush(); // Persist all managed Entities.
    }
}
