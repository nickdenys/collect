<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Comment;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory as Faker;

class LoadCommentData extends AbstractFixture implements OrderedFixtureInterface
{
    const COUNT = 3;

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 4; // The order in which fixture(s) will be loaded.
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $em)
    {
        $locale = 'en';
        $faker = Faker::create($locale);

        for ($commentCount = 0; $commentCount < self::COUNT; ++$commentCount) {
            $comment = new Comment();
            $em->persist($comment);
            $comment
                ->setReport($this->getReference('Report-1')) // Get reference from a previous Data Fixture.
                ->setUser($this->getReference("TestUser-${commentCount}")) // Get reference from a previous Data Fixture.
                ->setContent($faker->paragraph($nbSentences = 2, $variableNbSentences = true));
        }

        $em->flush(); // Persist all managed Entities.
    }
}
