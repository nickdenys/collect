<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\User;
use AppBundle\Traits\ContainerTrait;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory as Faker;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

class LoadUserData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    use ContainerTrait;

    const COUNT = 5;

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 2; // The order in which fixture(s) will be loaded.
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $em)
    {
        // Get userManager by implementing `ContainerAwareInterface`
        $userManager = $this->container->get('fos_user.user_manager');

        $locale = 'en';
        $faker = Faker::create($locale);

        $user = $userManager->createUser();
        $user
            ->setFirstName('John')
            ->setLastName('Doe')
            ->setUsername('johndoe')
            ->setEmail('johndoe@email.com')
            ->setPlainPassword('password')
            ->setRoles(array('ROLE_ADMINISTRATOR'));
        $this->addReference('JohnDoe', $user); // Reference for the next Data Fixture(s).
        $userManager->updateUser($user, true);

        for ($userCount = 0; $userCount < self::COUNT; ++$userCount) {
            $user = new User();
            $em->persist($user);
            $user
                ->setFirstName($faker->firstName())
                ->setLastName($faker->lastName())
                ->setUsername($faker->userName())
                ->setEmail($faker->safeEmail())
                ->setPlainPassword('password');
            $this->addReference("TestUser-${userCount}", $user); // Reference for the next Data Fixture(s).
            $userManager->updateUser($user, true);
        }

        $em->flush(); // Persist all managed Entities.
    }
}
