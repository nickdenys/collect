<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Report;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Faker\Factory as Faker;

class LoadReportData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 3; // The order in which fixture(s) will be loaded.
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $em)
    {
        $locale = 'en';
        $faker = Faker::create($locale);

        $filePath = '/home/vagrant/collect.local/www/web/to_upload/reports/';

        $report = new Report(null);
        $em->persist($report); // Manage Entity for persistence.
        $report
            ->setTitle('Report 1')
            ->setDescription($faker->paragraph($nbSentences = 5, $variableNbSentences = true))
            ->setLatitude('40.713435')
            ->setLongitude('-73.989251')
            ->setImageFile(new File($filePath.'report1.jpg', 'report1.jpg', 'image/jpeg', filesize($filePath.'report1.jpg'), null, true))
            ->setImageName('report1.jpg')
            ->setUser($this->getReference('JohnDoe')) // Get reference from a previous Data Fixture.
            ->setStatus($this->getReference('Status-Reported')); // Get reference from a previous Data Fixture.
        $this->addReference('Report-1', $report); // Reference for the next Data Fixture(s).

        $report = new Report(null);
        $em->persist($report); // Manage Entity for persistence.
        $report
            ->setTitle('Report 2')
            ->setDescription($faker->paragraph($nbSentences = 5, $variableNbSentences = true))
            ->setLatitude('40.729438')
            ->setLongitude('-74.006202')
            ->setImageFile(new File($filePath.'report2.jpg', 'report2.jpg', 'image/jpeg', filesize($filePath.'report2.jpg'), null, true))
            ->setImageName('report2.jpg')
            ->setUser($this->getReference('JohnDoe')) // Get reference from a previous Data Fixture.
            ->setStatus($this->getReference('Status-Confirmed')); // Get reference from a previous Data Fixture.
        $this->addReference('Report-2', $report); // Reference for the next Data Fixture(s).

        $report = new Report(null);
        $em->persist($report); // Manage Entity for persistence.
        $report
            ->setTitle('Report 3')
            ->setDescription($faker->paragraph($nbSentences = 5, $variableNbSentences = true))
            ->setLatitude('40.783986')
            ->setLongitude('-73.95597')
            ->setImageFile(new File($filePath.'report3.jpg', 'report3.jpg', 'image/jpeg', filesize($filePath.'report3.jpg'), null, true))
            ->setImageName('report3.jpg')
            ->setUser($this->getReference('JohnDoe')) // Get reference from a previous Data Fixture.
            ->setStatus($this->getReference('Status-Fixed')); // Get reference from a previous Data Fixture.
        $this->addReference('Report-3', $report); // Reference for the next Data Fixture(s).

        $em->flush(); // Persist all managed Entities.
    }
}
