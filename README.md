# Collect

Collect is een systeem waarbij de community meldingen doorgeeft van zwerfvuil in hun buurt. Deze meldingen worden opgeslagen in een databank en doorgestuurd naar de betreffende gemeente. Die gemeente heeft nu een handig overzicht van alle plaatsen waar zwerfvuil terechtkomt en kan zodanig gepast reageren binnen een bepaalde tijd.

---

### Info

|                      |                                            |
|----------------------|--------------------------------------------|
| Auteur               | Nick Denys                                 |
| Opleidingsonderdeel  | New Media Design & Development III         |
| Academiejaar         | 2015 - 2016                                |
| Opleiding            | Bachelor in de grafische en digitale media |
| Afstudeerrichting    | Multimediaproductie                        |
| Keuzeoptie           | proDEV                                     |
| Opleidingsinstelling | Arteveldehogeschool                        |
