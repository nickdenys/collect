Checklist
=========

| Status   | Aanduiding |
|:---------|:----------:|
| Aanwezig |      ✓     |
| Afwezig  |      ✗     |

| Academische Poster                   | Aanwezig |
|:-------------------------------------|:--------:|
| Afgedrukt op A3                      |     ✓    |
| PDF-bestand                          |     ✓    |
| Bestand op repository                |     ✓    |
| Spelling gecontroleerd               |     ✓    |
|--------------------------------------|----------|
| 1. Synopsis                          |     ✓    |
| 2. Doel                              |     ✓    |
| 3. Resultaat                         |     ✓    |
| 4. Technologieën                     |     ✓    |

| Presentatie                          | Aanwezig |
|:-------------------------------------|:--------:|
| PDF-bestand                          |     ✓    |
| Bestand op repository                |     ✓    |
| Spelling gecontroleerd               |     ✓    |

| Productiedossier                     | Aanwezig |
|:-------------------------------------|:--------:|
| PDF-bestand                          |     ✓    |
| Bestand op repository                |     ✓    |
|--------------------------------------|----------|
|  1.  Voorblad                        |     ✓    |
|  2.  Briefing                        |     ✓    |
|  3.  Repository                      |     ✓    |
|  4.  Planning                        |     ✓    |
|  5.  Ideaboard                       |     ✓    |
|  6.  Moodboard                       |     ✓    |
|  7.  Style Tiles                     |     ✓    |
|  8.  API                             |     ✓    |
|  8.1 Functionaliteiten               |     ✓    |
|  9.  Mobile Hybrid App               |     ✓    |
|  9.1 Functionaliteiten               |     ✓    |
|  9.2 Wireflow                        |     ✓    |
|  9.3 Screen Designs                  |     ✓    |
| 10.  Backoffice                      |     ✓    |
| 10.1 Functionaliteiten               |     ✓    |
| 10.2 Sitemap                         |     ✓    |
| 10.3 Wireframes                      |     ✗    |
| 10.4 Screen Designs                  |     ✓    |

| Timesheet                            | Aanwezig |
|:-------------------------------------|:--------:|
| Excel-bestand                        |     ✓    |
| Bestand op repository                |     ✓    |
| Per dag ingevuld                     |     ✓    |
| Totaal berekend in uren              |     ✓    |

| Werkstuk                             | Aanwezig |
|:-------------------------------------|:--------:|
| **Source Control Management**        |          |
|   Commits met prefixen               |     ✓    |
|   Feature Branches met postfix       |     ✓    |
|--------------------------------------|----------|
| **Werkstuk en technologieën**        |          |
|  *Database*                          |          |
|   Databasemodel                      |     ✓    |
|  *Server-side*                       |          |
|   Doctrine Entities                  |     ✓    |
|   Doctrine Fixtures                  |     ✓    |
|   Doctrine Migrations                |     ✓    |
|   Doctrine ORM                       |     ✓    |
|   Doctrine Repositories              |     ✓    |
|   Symfony 2.8 LTS                    |     ✓    |
|   Symfony Console Commands voor DB   |     ✓    |
|--------------------------------------|----------|
|  *API*                               |          |
|   CRUD: create                       |     ✓    |
|   CRUD: delete                       |     ✓    |
|   CRUD: read                         |     ✓    |
|   CRUD: update                       |     ✓    |
|   FOSRestBundle                      |     ✓    |
|   NelmioApiDocBundle                 |     ✓    |
|   NelmioCorsBundle                   |     ✓    |
|   PHPUnit                            |     ✓    |
|   PHPUnit-tests succesvol            |     ✗    |
|   RESTful                            |     ✓    |
|--------------------------------------|----------|
|  *Mobile Hybrid App*                 |          |
|   Automatisatie: Bower               |     ✓    |
|   Automatisatie: Gulp.js             |     ✓    |
|   Functionaliteit: Account           |     ✓    |
|   Functionaliteit: Rollen            |     ✓    |
|   Functionaliteit: Gamification      |     ✓    |
|   Functionaliteit: Leaderboard       |     ✓    |
|   Functionaliteit: Zwerfvuilmelding  |     ✓    |
|   Functionaliteit: Zwerfvuilstatus   |     ✓    |
|   Ionic Framework                    |     ✓    |
|   Ionic Framework + ngCordova        |     ✓    |
|   Sass (SCSS)                        |     ✓    |
|   Spelling gecontroleerd             |     ✓    |
|--------------------------------------|----------|
|  *Backoffice*                        |          |
|   Automatisatie: Bower               |     ✓    |
|   Automatisatie: Composer            |     ✓    |
|   Automatisatie: Gulp.js             |     ✓    |
|   BDD                                |     ✗    |
|   Behat                              |     ✗    |
|   Behat functionaliteiten in Gherkin |     ✗    |
|   Behat-tests succesvol              |     ✗    |
|   Bootstrap met Sass (SCSS)          |     ✓    |
|   Functionaliteit: Dashboard         |     ✓    |
|   Functionaliteit: Gebruikersbeheer  |     ✓    |
|   Functionaliteit: Rollen            |     ✓    |
|   Functionaliteit: Zwerfvuilstatus   |     ✓    |
|   Sass (SCSS)                        |     ✓    |
|   Spelling gecontroleerd             |     ✓    |
|   Twig-sjablonen                     |     ✓    |
